
package com.hibisca.transformers.hibisca.screens;

import android.support.v4.app.FragmentManager;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hibisca.transformers.hibisca.database.controllers.HumidityController;
import com.hibisca.transformers.hibisca.fragments.SelectPlantFragment;
import com.hibisca.transformers.hibisca.MenuActivity;
import com.hibisca.transformers.hibisca.pojo.Humidity;
import com.hibisca.transformers.hibisca.pojo.Plant;
import com.hibisca.transformers.hibisca.MenuActivity;
import com.hibisca.transformers.hibisca.pojo.Humidity;
import com.hibisca.transformers.hibisca.R;

/**
 * This screen will give the user the option of checking the humidity of
 * their current location
 */

public class HumidityScreen extends MenuActivity implements SensorEventListener, SelectPlantFragment.SelectPlantListener{


    private SensorManager humiditySensorManager;
    private HumidityController controller;
    private Sensor humiditySensor;
    private TextView displayHumidity;
    private ImageView raindrop;
    private Button getHumidity;
    private Button readForPlant;
    private double humidityReading;
    private Plant selectedPlant = null;
    private Button deleteReading;


    /**
     * Initializes the various variables that will be needed for the screen
     * as well setting the listeners for the button to turning the sensor on
     * and off
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_humidity_screen);
        setTitle(R.string.titleHumidity);
        humiditySensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        controller = new HumidityController(this);
        humiditySensor = humiditySensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);
        displayHumidity = findViewById(R.id.displayHumidity);
        getHumidity = (Button) findViewById(R.id.btnHumidity);
        readForPlant = (Button) findViewById(R.id.btnReadForPlant);
        raindrop = (ImageView) findViewById(R.id.rain_drop);
        deleteReading = (Button)findViewById(R.id.deleteReading);

        getHumidity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getHumidity.setText(R.string.btnStopReading);
                humiditySensorManager.registerListener(HumidityScreen.this, humiditySensor, SensorManager.SENSOR_DELAY_NORMAL);
            }
        });

        getHumidity.setOnLongClickListener(
                new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        getHumidity.setText(R.string.btnBeginReading);
                        humiditySensorManager.unregisterListener(HumidityScreen.this, humiditySensor);
                        if (selectedPlant != null)
                            storeValue(humidityReading);
                        return true;
                    }
                });

        readForPlant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager manager = getSupportFragmentManager();
                SelectPlantFragment selectPlant = new SelectPlantFragment();
                selectPlant.setCancelable(true);
                selectPlant.show(manager, "humidity");
            }
        });

        deleteReading.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteHumidityReading();
            }
        });


    }

    /**
     * Retreives the selected plant
     * @param plant the plant the user selected
     */
    public void selectPlant(Plant plant) {
        Log.v("SELECTED", "SEL: " + plant.getPlantName());
        selectedPlant = new Plant(plant);
    }

    /**
     * Stores the current reading for humidity in the database
     * @param humidityReading the last known humidity reading
     * @return true if stored
     */
    public boolean storeValue(double humidityReading) {

        Humidity humidity = new Humidity(selectedPlant.getPlantID(), humidityReading);
        HumidityController controller = new HumidityController(this);
        controller.insertHumidityReading(humidity);
        selectedPlant = null;
        return true;
    }

    /**
     * Removes a humidity reading from the database
     */
    public void deleteHumidityReading() {
        if(selectedPlant == null)
            Toast.makeText(this, R.string.selectPlantToDelete, Toast.LENGTH_SHORT).show();
        else {
            controller.deleteHumidityReading(selectedPlant.getPlantID());
        }
    }

    /**
     * Is called when the accuracy of the sensor is changed
     * @param sensor
     * @param accuracy
     */
    @Override
    public final void onAccuracyChanged(Sensor sensor, int accuracy)
    {

    }

    /**
     * Is called when the humidity value changes
     * Changes the text in the raindrop as well as changes the
     * opacity of the raindrop image
     * @param event
     */
    @Override
    public final void onSensorChanged(SensorEvent event)
    {
        displayHumidity.setText(Float.toString(event.values[0]) + "%");
        humidityReading = Math.round(event.values[0]);
        if(event.values[0]<6.25)
        raindrop.setAlpha((float) 0.0625);
        else
        raindrop.setAlpha(event.values[0] / 100);
    }

    /**
     * When Paused, the Sensor Manager stops looking for the humidity in the area
     */
    @Override
    protected void onPause()
    {
        super.onPause();
        humiditySensorManager.unregisterListener(this);
    }

    @Override
    protected void onStop(){
        Log.d("Stop", "Stop");
        super.onStop();
    }
    @Override
    protected void onDestroy() {
        Log.d("Destroy", "Destroy");
        super.onDestroy();
    }
}

