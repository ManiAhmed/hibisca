package com.hibisca.transformers.hibisca.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hibisca.transformers.hibisca.pojo.Plant;
import com.hibisca.transformers.hibisca.R;

import java.util.List;

/**
 * Created by cstuser on 4/19/2018.
 */

public class MyPlantsListAdapter extends ArrayAdapter<Plant> {
   private Context context;

    /**
     * Constructor for Plants list adapter
     * @param context context of the calling activity
     * @param plantList list of user's plants
     */
   public MyPlantsListAdapter(Context context, List<Plant> plantList){
       super(context, 0, plantList);
       this.context = context;
   }

   private class PlantItem {
       ImageView plantImage;
       TextView plantName;
   }

    /**
     * Returns a plant view for list view
     * @param position position the specified index
     * @param convertView the view to be changed
     * @param container the parent viewgroup
     * @return converted plant view
     */
   public View getView(int position, View convertView, ViewGroup container) {
       //create inflater and inflate plant listview
       if(convertView == null) {
           LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
          convertView = inflater.inflate(R.layout.plants_list_view, container, false);
       }
       //associate them in the layout and get plant according to position in list (this)

           //set the text/name of the plant
       //PlantItem plantItem = new PlantItem();
       Plant plant = this.getItem(position);
       ImageView plantImage = (ImageView)convertView.findViewById(R.id.list_image);
       TextView plantName = (TextView) convertView.findViewById(R.id.list_text);
       plantImage.setImageResource(R.drawable.ic_plants);
       plantName.setText(plant.getPlantName());
//       convertView.setOnClickListener(new View.OnClickListener(){
//           @Override
//           public void onClick(View view) {
//
//           }
//       });
       return convertView;
   }
}
