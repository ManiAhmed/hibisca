package com.hibisca.transformers.hibisca.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.hibisca.transformers.hibisca.R;

import java.util.List;

/**
 * Creates a simple listview that will be used to display daily or all events
 * available in the database.
 */
public class EventAdapter extends ArrayAdapter
{
    private final Activity c;
    private int layoutResourceId;
    private final List<String> data;

    /**
     * Arg-contrustor
     * @param context the context building the listview
     * @param layoutId the layout of the listView.
     * @param list the collection of data/information to be displayed in the list
     */
    public EventAdapter (Activity context, int layoutId, List<String> list)
    {
        super(context, layoutId, list);
        this.layoutResourceId = layoutId;
        this.data = list;
        this.c = context;
    }

    /**
     * Specifies how the output should be displayed
     * @param index the position where the data/row is to be created and placed in relation to the view.
     * @param row the view associated with the current data
     * @param parent the container(ListView widget) each row in the listView will be added to
     * @return returns the row to be added to the container.
     */
    @Override
    public View getView(int index, View row, ViewGroup parent)
    {
        LayoutInflater lI = c.getLayoutInflater();
        row = lI.inflate(layoutResourceId,parent,false);
        TextView text = (TextView) row.findViewById(R.id.list_row_text);
        text.setText(data.get(index).toString());//output set here
        return text;
    }
}
