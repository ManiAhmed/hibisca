package com.hibisca.transformers.hibisca.screens;

import android.app.DialogFragment;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.hibisca.transformers.hibisca.interfaces.OnLoginButtonListener;
import com.hibisca.transformers.hibisca.pojo.User;
import com.hibisca.transformers.hibisca.R;

/**
 * This fragment allows user who are already signed up to quickly
 * log in to the application
 */

public class LoginScreen extends DialogFragment {

    EditText username;
    EditText password;
    Button ok;
    Button cancel;

    /**
     * Default constructor
     */
    public LoginScreen()
    {

    }

    /**
     * Initializes all the variables as well as the listeners for the ok and cancel buttons
     * Returns the login fragment for the user to interact with
     * Error Handling to be added to make sure it is an active user
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return login = the login fragment
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View login = inflater.inflate(R.layout.activity_login_screen, container, false);
        getDialog().setTitle(R.string.titleLogin);
        username = (EditText) login.findViewById(R.id.txtUsername);
        password = (EditText) login.findViewById(R.id.txtPassword);
        ok = (Button) login.findViewById(R.id.btnOk);
        cancel = (Button) login.findViewById(R.id.btnCancel);

        final MediaPlayer fwd = MediaPlayer.create(getActivity(), R.raw.forward);
        final MediaPlayer bck = MediaPlayer.create(getActivity(), R.raw.backward);

        ok.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                fwd.start();
                if(username.getText().toString().isEmpty())
                    Toast.makeText(getActivity(), R.string.txtMissingUsername, Toast.LENGTH_LONG).show();

                else if(password.getText().toString().isEmpty())
                    Toast.makeText(getActivity(), R.string.txtMissingPassword, Toast.LENGTH_LONG).show();

                else
                {
                    User returningUser = new User();

                    String strUsername = username.getText().toString();
                    String strPassword = password.getText().toString();

                    returningUser.setUsername(strUsername);
                    returningUser.setPassword(strPassword);

                    OnLoginButtonListener loginButtonListener = (OnLoginButtonListener) getActivity();
                    loginButtonListener.onFinishLogin(returningUser);
                }
            }
        });

        cancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                bck.start();
                dismiss();
            }
        });

        return login;
    }


}
