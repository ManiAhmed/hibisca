package com.hibisca.transformers.hibisca.screens;

import android.graphics.Color;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hibisca.transformers.hibisca.database.controllers.ThermometerController;
import com.hibisca.transformers.hibisca.fragments.SelectPlantFragment;
import com.hibisca.transformers.hibisca.MenuActivity;
import com.hibisca.transformers.hibisca.pojo.Plant;
import com.hibisca.transformers.hibisca.MenuActivity;
import com.hibisca.transformers.hibisca.pojo.Temperature;
import com.hibisca.transformers.hibisca.R;

/**
 * Created by cstuser on 3/9/2018.
 */

public class TemperatureScreen extends MenuActivity implements SensorEventListener, SelectPlantFragment.SelectPlantListener{

    private static SensorManager temperatureSensorManager;
    private ThermometerController controller;
    private static Sensor temperatureSensor;
    private TextView displayTemp;
    private Button readTemperature;
    private ImageView tempLevel;
    private ImageView tempCircle;
    private double temperatureReading;
    private Button readForPlant;
    private Plant selectedPlant = null;
    private Button deleteReading;

    /**
     * Sets up activity and layout for temperature sensor activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.temperature_layout);
        setTitle(R.string.titleTemperature);
        controller = new ThermometerController(this);
        readTemperature = (Button)findViewById(R.id.btnTemperature);
        tempLevel = (ImageView)findViewById(R.id.templvl1);
        displayTemp = (TextView)findViewById(R.id.displayTemp);
        tempCircle = (ImageView)findViewById(R.id.tempcircle);
        readForPlant = (Button) findViewById(R.id.btnReadForPlant);
        deleteReading = (Button)findViewById(R.id.deleteReading);

        temperatureSensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        temperatureSensor = temperatureSensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);

        readTemperature.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                readTemperature.setText(R.string.btnStopReading);
                temperatureSensorManager.registerListener(TemperatureScreen.this, temperatureSensor, SensorManager.SENSOR_DELAY_NORMAL);
            }
        });

        readTemperature.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                readTemperature.setText(R.string.btnBeginReading);
                if (selectedPlant != null)
                    storeValue(temperatureReading);
                temperatureSensorManager.unregisterListener(TemperatureScreen.this, temperatureSensor );
                return true;
            }
        });

        readForPlant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager manager = getSupportFragmentManager();
                SelectPlantFragment selectPlant = new SelectPlantFragment();
                selectPlant.setCancelable(true);
                selectPlant.show(manager, "temperature");
            }
        });

        deleteReading.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteTemperatureReading();
            }
        });

    }

    /**
     * Retreives the selected plant
     * @param plant the plant the user selected
     */
    public void selectPlant(Plant plant) {
        Log.v("SELECTED", "SEL: " + plant.getPlantName());
        selectedPlant = new Plant(plant);
    }

    private boolean storeValue(double temperatureReading) {
        Temperature temperature = new Temperature(selectedPlant.getPlantID(), temperatureReading);
        ThermometerController controller = new ThermometerController(this);
        controller.insertTemperatureReading(temperature);
        return true;
    }

    /**
     * Removes a temperature reading from the database
     */
    public void deleteTemperatureReading() {
        if(selectedPlant == null)
            Toast.makeText(this, R.string.selectPlantToDelete, Toast.LENGTH_SHORT).show();
        else {
            controller.deleteThermometerReading(selectedPlant.getPlantID());
        }
    }

    /**
     * Changes state of thermometer with different temperature readings
     * @param sensorEvent temperature sensor change event
     */
    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        float tempValue = sensorEvent.values[0];
        temperatureReading = sensorEvent.values[0];
        displayTemp.setText(Float.toString(tempValue));

        float hue;
        float scale;
        if(tempValue > 50) {
            hue = 0;
            scale = 7.4f;
        }
        else if(tempValue < -50) {
            hue = 250;
            scale = 0.5f;
        }
        else {
            hue = (float)(-8.2*Math.sqrt(9.2949*(tempValue+50))+250);
            scale = (float)(-0.74*Math.sqrt(-tempValue+50) + 7.4);
        }
        float[] hsv = {hue, 0.96f, 1};
        int color = Color.HSVToColor(hsv);
        tempLevel.setColorFilter(color);
        tempCircle.setColorFilter(color);
        tempLevel.setScaleY(scale);
    }

    /**
     *
     * @param tempSensor temperature sensor
     * @param accuracy accuracy value
     */
    @Override
    public void onAccuracyChanged(Sensor tempSensor, int accuracy) {

    }

    /**
     * Unregisters sensor manager when activity is paused
     */
    @Override
    protected void onPause() {
        super.onPause();
        temperatureSensorManager.unregisterListener(this);
    }

    /**
     * Registers sensor manager when activity is resumed
     */
    @Override
    protected void onResume() {
        super.onResume();
    }
}
