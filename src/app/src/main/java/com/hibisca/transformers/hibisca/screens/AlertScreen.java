package com.hibisca.transformers.hibisca.screens;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.Toast;

import com.hibisca.transformers.hibisca.database.controllers.AlertsController;


import com.hibisca.transformers.hibisca.fragments.EventFragment;
import com.hibisca.transformers.hibisca.MenuActivity;
import com.hibisca.transformers.hibisca.R;
import com.hibisca.transformers.hibisca.services.NotifyService;

import java.util.Calendar;

/**
 * This class will be our base class/activity
 * to view a calendar and select/place events
 * specific to the application and will use a service
 * to display a notifcation on the event date show on that calendar
 */
public class AlertScreen extends MenuActivity implements EventFragment.OnFragmentButtonListener
{
    /**
     * Insance variables used in the global scope.
     */
    private CalendarView alerts;
    private AlertsController userAlerts;
    private int alrtYear;
    private int alrtMonth;
    private int alrtDay;

    /**
     *  The basic instantiation of the activty with its
     *  event handlers for a given date that has been selected.
     * @param savedInstanceState state of the emulator
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.alerts_calendar);
        final CalendarView selectDate = (CalendarView)findViewById(R.id.alrtCalendar);
        final Button viewAll = (Button)findViewById(R.id.viewAll);

        viewAll.setOnClickListener(new View.OnClickListener()
        {
            /**
             *  Will display all the current events logged in the databse
             *  when heard.
             * @param view the button that was pressed
             */
            @Override
            public void onClick(View view)
            {
                Intent eventList = new Intent(AlertScreen.this, EventList.class);
                startActivity(eventList);
            }
        });
        this.userAlerts = new AlertsController(this.getApplicationContext());
        alerts = (CalendarView)findViewById(R.id.alrtCalendar);
        alerts.setOnDateChangeListener(new CalendarView.OnDateChangeListener()
        {
            /**
             * An event listener, listening to which day has been pressed/clicked on
             * from out calendar view.
             * @param view the calendar where the event occured
             * @param year the year associated with the day that was pressed.
             * @param month the month the day was pressed.
             * @param dayOfMonth the day that was pressed.
             */
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth)
            {
                alrtYear = year;
                alrtMonth = month;
                alrtDay = dayOfMonth;
                AlertDialog.Builder dateDialog = new AlertDialog.Builder(AlertScreen.this);
                dateDialog.setTitle(R.string.dialogEvent);
                dateDialog.setMessage(R.string.dialogMessage);
                dateDialog.setCancelable(false);
                dateDialog.setPositiveButton(R.string.btnAddAlert,new DialogInterface.OnClickListener()
                {
                    /**
                     * Display a fragment that enable the user to add an event to the day that
                     * was selected.
                     * @param dialog the dialog being inflated
                     * @param id the id of the positive button inflated into the dialog
                     */
                            public void onClick(DialogInterface dialog, int id)
                            {
                                selectDate.setVisibility(View.INVISIBLE);
                                viewAll.setVisibility(View.INVISIBLE);
                                FragmentManager fm = getFragmentManager();
                                FragmentTransaction ft = fm.beginTransaction();
                                EventFragment ef = new EventFragment();
                                ft.replace(R.id.calendarFrame, ef);
                                ft.commit();
                                dialog.cancel();
                            }
                        });
                dateDialog.setNeutralButton(R.string.btnTodayEvents, new DialogInterface.OnClickListener()
                {
                    /**
                     * Display a fragment that enable the user to view all events currently placed on this day.
                     * @param dialog the dialog being inflated
                     * @param id the id of the positive button inflated into the dialog
                     */
                    @Override
                    public void onClick(DialogInterface dialog, int id)
                    {
                        Intent eventList = new Intent(AlertScreen.this, DailyEventList.class);
                        eventList.putExtra("today",alrtDay);
                        eventList.putExtra("thisMonth",alrtMonth);
                        eventList.putExtra("thisYear",alrtYear);
                        startActivity(eventList);
                    }
                });
                dateDialog.setNegativeButton(R.string.cancel,new DialogInterface.OnClickListener()
                {
                    /**
                     * Return to the alertscreen/calendar
                     * @param dialog the dialog being inflated
                     * @param id the id of the positive button inflated into the dialog
                     */
                            public void onClick(DialogInterface dialog,int id)
                            {
                                dialog.cancel();
                            }
                });
                AlertDialog alertDialog = dateDialog.create();
                alertDialog.show();
            }
        });
     }

    /**
     * Receive the data from the EventFragment if the user chose the add a event button
     * on the dialog frgament. This will process the data and attempt to insert it into the database.
     * A toast will be used to display the success of whether or not the alert could be added. Lastly
     * this will add/update the service by adding an additional event to the systens calendar.
     * @param strID the Id the user selected for the alert.
     * @param strDesc the description of the alert defining the event.
     */
    @Override
    public void onFragmentButtonClicked(String strID, String strDesc)
    {
        long success = userAlerts.insertAlert(alrtDay,alrtMonth,alrtYear,strDesc,strID);
        if(success == -1)
        {
            Toast.makeText(this, R.string.txtAddAlertErr,Toast.LENGTH_LONG).show();
        }
        else
        {
            Toast.makeText(this, R.string.txtAddAlert,Toast.LENGTH_LONG).show();
        }

        Toast.makeText(this, R.string.txtAddAlert ,Toast.LENGTH_LONG).show();

        Intent myIntent = new Intent(this , NotifyService.class);
        myIntent.putExtra("alertID",strID);
        myIntent.putExtra("strDesc", strDesc);

        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);

        PendingIntent pendingIntent = PendingIntent.getService(this, 0, myIntent, 0);


        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_MONTH, alrtDay);
        calendar.set(Calendar.MONTH, alrtMonth);
        calendar.set(Calendar.YEAR, alrtYear);

        alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(),1000*60*60*24 , pendingIntent);
    }

    /**
     * This will be used to make sure that anytime the user returns to this screen that the contents
     * are correctly displayed.
      */
    @Override
    public void onResume()
    {
        super.onResume();
        final CalendarView selectDate = (CalendarView)findViewById(R.id.alrtCalendar);
        final Button viewAll = (Button)findViewById(R.id.viewAll);
        selectDate.setVisibility(View.VISIBLE);
        viewAll.setVisibility(View.VISIBLE);
    }

}
