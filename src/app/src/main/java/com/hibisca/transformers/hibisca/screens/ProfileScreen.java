package com.hibisca.transformers.hibisca.screens;

import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.constraint.ConstraintLayout;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.hibisca.transformers.hibisca.database.controllers.ProfileController;
import com.hibisca.transformers.hibisca.fragments.UpdateUserFragment;
import com.hibisca.transformers.hibisca.interfaces.OnUpdateButtonListener;
import com.hibisca.transformers.hibisca.MenuActivity;
import com.hibisca.transformers.hibisca.pojo.User;
import com.hibisca.transformers.hibisca.pojo.UserSession;
import com.hibisca.transformers.hibisca.R;

import java.io.IOException;

/**
 * Sets up the users profile and maintains basic
 * user data. It will also allow the user to
 * navigate from their profile screen to their
 * current plant inventory, alter user information
 * and enable them to log out or the app.
 */
public class ProfileScreen extends MenuActivity implements OnUpdateButtonListener
{
    private int PICK_IMAGE_REQUEST = 1;
    private ImageButton prflImg;
    private Button yourPlnts;
    private Button edtPrfl;
    private Button lgOut;
    ProfileController profileController;

    /**
     * Clicking the users profile picture(ImageButton)
     * will create an implicit intent to enable them
     * to select from their local storage the picture they
     * wish to use as their profile picture.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_screen);
        setTitle(R.string.titleProfile);
        profileController = new ProfileController(this);

        prflImg = (ImageButton)findViewById(R.id.profileImage);

        final Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);

        prflImg.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                startActivityForResult(Intent.createChooser(intent, "Select Your Profile Picture"), PICK_IMAGE_REQUEST);
            }
        });

        yourPlnts = (Button)findViewById(R.id.yourPlants);
        yourPlnts.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {

                Intent intent = new Intent(getBaseContext(),MyPlantsScreen.class);
                startActivity(intent);
            }
        });

        edtPrfl = (Button)findViewById(R.id.editAccout);
        edtPrfl.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                FragmentManager fragmentManager = getFragmentManager();
                UpdateUserFragment updateDialog = new UpdateUserFragment();
                updateDialog.setCancelable(false);
                updateDialog.show(fragmentManager, "login Dialog");
                ///first we need to gather information from database to be able to update content.
                /*Toast t = Toast.makeText(getBaseContext(),"Placeholder Test Toast", Toast.LENGTH_LONG);
                t.show();*/
            }
        });

        lgOut = (Button)findViewById(R.id.logOut);
        lgOut.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(getBaseContext(),HomeScreen.class);//can add extra sounds here
                startActivity(intent);
                finish();
            }
        });
    }

    /**
     *  Listens to the ImageButtons implicit request(intent) to get
     *  an image from the users local storage and set it to be their
     *  profile pciture.
     * @param requestCode Used to verify that the call to the intent was made.
     * @param resultCode Used to verify that the call to the intent came back.
     * @param data The data/filepath to the image used on local storage for the
     *             users profile picture.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null)
        {
            Uri uri = data.getData();
            try
            {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);

                prflImg = (ImageButton) findViewById(R.id.profileImage);

                ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams) prflImg.getLayoutParams();
                params.height = 400;
                params.width = 400;
                prflImg.setLayoutParams(params);
                prflImg.setScaleType(ImageView.ScaleType.FIT_XY);

                prflImg.setImageBitmap(bitmap);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

    /**
     * Replaces the current user info with the updated
     * version from the database
     * @param user
     */
    @Override
    public void onFinishUpdate(User user) {
        profileController.updateUser(UserSession.getInstance(), user);
        UserSession.getInstance().setUsername(user.getUsername());
        UserSession.getInstance().setFirstName(user.getFirstName());
        UserSession.getInstance().setLastName(user.getLastName());
        UserSession.getInstance().setEmail(user.getEmail());
        UserSession.getInstance().setPostalCode(user.getPostalCode());
        UserSession.getInstance().setPassword(user.getPassword());
    }
}
