package com.hibisca.transformers.hibisca.screens;

import android.os.Bundle;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;
import com.hibisca.transformers.hibisca.database.controllers.HumidityController;
import com.hibisca.transformers.hibisca.database.controllers.LightController;
import com.hibisca.transformers.hibisca.database.controllers.PlantInfoController;
import com.hibisca.transformers.hibisca.database.controllers.ThermometerController;
import com.hibisca.transformers.hibisca.MenuActivity;
import com.hibisca.transformers.hibisca.pojo.PlantInfo;
import com.hibisca.transformers.hibisca.R;

import java.util.List;

public class PlantInfoScreen extends MenuActivity {
    private String lightReading;
    private String humidityReading;
    private String tempReading;
    private ImageView plantImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plant_info_screen);

        String spec = getIntent().getStringExtra("Spcies");
        String plantID = getIntent().getStringExtra("ID");
        String plantName = getIntent().getStringExtra("PlantName");

        setTitle(plantName);
        setPlantInformation(spec, plantID, plantName);
    }

    /**
     * Initializes all the data to be displayed for the associated plant
     * @param spec the plant species
     * @param plantID the plantID
     * @param plantName the plant name
     */
    private void setPlantInformation(String spec, String plantID, String plantName) {
        PlantInfoController plantInfoController = new PlantInfoController(this);
        PlantInfo plant = plantInfoController.getPlantInformation(spec);

        setPlantImage(plant);
        setPlantSpecies(plant);
        setPlantFertilizer(plant);
        setLightRequirements(plant);
        setHumidityRequirements(plant);
        setTemperatureRequirements(plant);

        setCurrentLight(plantID);
        setCurrentHumidity(plantID);
        setCurrentTemperature(plantID);
    }

    public void setPlantImage(PlantInfo plant) {
        plantImage = findViewById(R.id.plantImageView);
        switch (plant.getSpecies()) {
            case "Hibiscus":
                plantImage.setImageResource(R.drawable.hibiscus);
                break;
            case "Araceae":
                plantImage.setImageResource(R.drawable.araceae);
                break;
            case "Calathea":
                plantImage.setImageResource(R.drawable.calathea);
                break;
            case "Dizygotheca":
                plantImage.setImageResource(R.drawable.dizygotheca);
                break;
            case "Dracaena":
                plantImage.setImageResource(R.drawable.dracaena);
                break;

            default:
                plantImage.setImageResource(R.drawable.ic_plants);
        }
    }

    /**
     * Sets the light requirements for a plant
     * @param plant the specified plant
     */
    public void setLightRequirements(PlantInfo plant) {
        String plantLightText = "<b>Light requirements: </b>" + Integer.toString(plant.getLight()) + "lx";
        TextView plantLight = (TextView) findViewById(R.id.plantLightTextView);
        plantLight.setText(Html.fromHtml(plantLightText, Html.FROM_HTML_MODE_LEGACY));
    }

    /**
     * Sets the humidity requirements for a plant
     * @param plant the specified plant
     */
    public void setHumidityRequirements(PlantInfo plant) {
        String plantHumidityText = "<b>Humidity requirements: </b>" + Integer.toString(plant.getHumidity()) + "%";
        TextView plantHumidity = (TextView) findViewById(R.id.plantHumidityTextView);
        plantHumidity.setText(Html.fromHtml(plantHumidityText, Html.FROM_HTML_MODE_LEGACY));
    }

    /**
     * Sets the temperature requirements for a plant
     * @param plant the specified plant
     */
    public void setTemperatureRequirements(PlantInfo plant) {
        String plantTempText = "<b>Temperature requirements: </b>" + Integer.toString(plant.getTemp()) + "C";
        TextView plantTemp = (TextView) findViewById(R.id.plantTempTextView);
        plantTemp.setText(Html.fromHtml(plantTempText, Html.FROM_HTML_MODE_LEGACY));
    }

    /**
     * Sets the species for a plant
     * @param plant the specified plant
     */
    public void setPlantSpecies(PlantInfo plant) {
        String plantSpeciesText = "<b>Species: </b>" + plant.getSpecies();
        TextView plantSpecies = (TextView) findViewById(R.id.plantSpeciesTextView);
        plantSpecies.setText(Html.fromHtml(plantSpeciesText, Html.FROM_HTML_MODE_LEGACY));
    }

    /**
     * Sets the fertilizer requirements for a plant
     * @param plant the specified plant
     */
    public void setPlantFertilizer(PlantInfo plant) {
        String plantFertilizerText = "<b>Fertilizer: </b>" + plant.getFertilizer();
        TextView plantFertilizer = (TextView) findViewById(R.id.plantFertilizerTextView);
        plantFertilizer.setText(Html.fromHtml(plantFertilizerText, Html.FROM_HTML_MODE_LEGACY));
    }

    /**
     * Sets the current light readings for a plant
     * @param plantID the specified plant's ID
     */
    public void setCurrentLight(String plantID) {
        LightController lightController = new LightController(this);
        try {
            List<Double> lightReadings = lightController.getLightReadings(plantID);
            lightReading = lightReadings.get(lightReadings.size() - 1) + "lx";
        }

        catch (IndexOutOfBoundsException e) {
            lightReading = "No light readings found!";
        }

        String lightReadingText = "<b>Current light reading: </b>" + lightReading;
        TextView currentLight = (TextView) findViewById(R.id.currentLightTextView);
        currentLight.setText(Html.fromHtml(lightReadingText, Html.FROM_HTML_MODE_LEGACY));
    }

    /**
     * Sets the current temperature readings for a plant
     * @param plantID the specified plant's ID
     */
    public void setCurrentTemperature(String plantID) {
        ThermometerController tempController = new ThermometerController(this);
        try {
            List<Double> tempReadings = tempController.getTemperatureReadings(plantID);
            tempReading = tempReadings.get(tempReadings.size() - 1) + "C";
        }

        catch (IndexOutOfBoundsException e) {
            tempReading = "No temperature readings found!";
        }

        String tempReadingText = "<b>Current temperature reading: </b>" + tempReading;
        TextView currentTemp = (TextView) findViewById(R.id.currentTempTextView);
        currentTemp.setText(Html.fromHtml(tempReadingText, Html.FROM_HTML_MODE_LEGACY));
    }

    /**
     * Sets the current humidity readings for a plant
     * @param plantID the specified plant's ID
     */
    public void setCurrentHumidity(String plantID) {
        HumidityController humidityController = new HumidityController(this);

        try {
            List<Double> humidityReadings = humidityController.getHumidityReadings(plantID);
            humidityReading = humidityReadings.get(humidityReadings.size() - 1) + "%";
        }

        catch (IndexOutOfBoundsException e) {
            humidityReading = "No humidity readings found!";
        }

        String humidityReadingText = "<b>Current humidity reading: </b>" + humidityReading;
        TextView currentHumidity = (TextView) findViewById(R.id.currentHumidityTextView);
        currentHumidity.setText(Html.fromHtml(humidityReadingText, Html.FROM_HTML_MODE_LEGACY));
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        finish();
    }
}
