package com.hibisca.transformers.hibisca.screens;

import android.graphics.Color;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.hibisca.transformers.hibisca.MenuActivity;
import com.hibisca.transformers.hibisca.R;
import com.hibisca.transformers.hibisca.database.controllers.HumidityController;
import com.hibisca.transformers.hibisca.database.controllers.LightController;
import com.hibisca.transformers.hibisca.database.controllers.PressureController;
import com.hibisca.transformers.hibisca.database.controllers.ThermometerController;
import com.hibisca.transformers.hibisca.fragments.SelectPlantFragment;
import com.hibisca.transformers.hibisca.pojo.Humidity;
import com.hibisca.transformers.hibisca.pojo.Plant;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.Arrays;
import java.util.List;

public class GraphScreen extends MenuActivity implements SelectPlantFragment.SelectPlantListener
{

    private Plant selectedPlant = null;
    private GraphView graph;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.graph_screen);
        Button readForPlant = (Button) findViewById(R.id.selectPlant);
        graph = (GraphView) findViewById(R.id.graph);

        readForPlant.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                FragmentManager manager = getSupportFragmentManager();
                SelectPlantFragment selectPlant = new SelectPlantFragment();
                selectPlant.setCancelable(true);
                selectPlant.show(manager, "graph");
            }
        });
    }

    /**
     * Retreives the selected plant
     * @param plant the plant the user selected
     */
    public void selectPlant(Plant plant)
    {
        Log.v("SELECTED", "SEL: " + plant.getPlantName());
        selectedPlant = new Plant(plant);
        addSeries(selectedPlant);
    }

    /**
     *  This method will graph all available
     *  data(light humidity pressure temperature) for any given
     *  plant in the database but specifically for the one selected by the user.
     * @param p the plant that the user selected in the select plant fragment.
     */
    public void addSeries(Plant p)
    {
        TextView plant = (TextView)findViewById(R.id.graphPlant);
        plant.setText(p.getPlantName());
        //Instantiate all the necessary database controllers for each sensor
        HumidityController humidityReadings = new HumidityController(GraphScreen.this);
        LightController lightReadings = new LightController(GraphScreen.this);
        PressureController pressureReadings = new PressureController(GraphScreen.this);
        ThermometerController thermalReadings = new ThermometerController(GraphScreen.this);

        //Verify that a plant was selected
        if(p != null)
        {
            //retrieve all the values for each sensor
            List<Double> humidRead = humidityReadings.getHumidityReadings(p.getPlantID());
            List<Double> lightRead = lightReadings.getLightReadings(selectedPlant.getPlantID());
            List<Double> pressRead = pressureReadings.getPressureReadings(selectedPlant.getPlantID());
            List<Double> tempRead = thermalReadings.getTemperatureReadings(selectedPlant.getPlantID());

            //Instantiate the number of graph points for each sensor
            DataPoint[] humidityDataPoint = new DataPoint[humidRead.size()];
            DataPoint[] lightDataPoint = new DataPoint[lightRead.size()];
            DataPoint[] pressDataPoint = new DataPoint[pressRead.size()];
            DataPoint[] tempDataPoint = new DataPoint[tempRead.size()];

            //Places all the values for each sensor as coordinate pairs
            for (int i = 0; i < humidRead.size(); i++)
            {
                humidityDataPoint[i] = new DataPoint(i, humidRead.get(i));
            }

            for (int i = 0; i < lightRead.size(); i++)
            {
               lightDataPoint[i] = new DataPoint(i, lightRead.get(i));
            }

            for (int i = 0; i < pressRead.size(); i++)
            {
                pressDataPoint[i] = new DataPoint(i, pressRead.get(i));
            }

            for (int i = 0; i < tempRead.size(); i++)
            {
               tempDataPoint[i] = new DataPoint(i, tempRead.get(i));
            }
            //creates the lines based on each sensore
            LineGraphSeries<DataPoint> humidity = new LineGraphSeries<DataPoint>(humidityDataPoint);
            LineGraphSeries<DataPoint> light = new LineGraphSeries<DataPoint>(lightDataPoint);
            LineGraphSeries<DataPoint> pressure = new LineGraphSeries<DataPoint>(pressDataPoint);
            LineGraphSeries<DataPoint> temperature = new LineGraphSeries<DataPoint>(tempDataPoint);

            //styles each line so that sensor data can be differentiated
            humidity.setTitle("Humidity Level");
            humidity.setColor(getBaseContext().getResources().getColor(R.color.backgroundBlue));
            humidity.setDrawDataPoints(true);
            humidity.setDataPointsRadius(10);
            humidity.setThickness(8);

            light.setTitle("Lux Level");
            light.setColor(getBaseContext().getResources().getColor(R.color.icon));
            light.setDrawDataPoints(true);
            light.setDataPointsRadius(10);
            light.setThickness(8);

            pressure.setTitle("Pressure Level");
            pressure.setColor(Color.WHITE);
            pressure.setDrawDataPoints(true);
            pressure.setDataPointsRadius(10);
            pressure.setThickness(8);

            temperature.setTitle("Temperature Level");
            temperature.setColor(Color.RED);
            temperature.setDrawDataPoints(true);
            temperature.setDataPointsRadius(10);
            temperature.setThickness(8);

            //adds graph lines based on sensor
            graph.addSeries(humidity);
            graph.addSeries(light);
            graph.addSeries(pressure);
            graph.addSeries(temperature);

            //set the view constraints of the graph
            graph.getViewport().setYAxisBoundsManual(true);
            //graph.getViewport().setMinY(0);
            //graph.getViewport().setMaxY(650);

            graph.getViewport().setXAxisBoundsManual(true);
            graph.getViewport().setMinX(0);
            //graph.getViewport().setMaxX(30);

            graph.getViewport().setScrollable(true); // enables horizontal scrolling
            graph.getViewport().setScrollableY(true); // enables vertical scrolling
            graph.getViewport().setScalable(true); // enables horizontal zooming and scrolling
            graph.getViewport().setScalableY(true); // enables vertical zooming and scrolling
        }
    }
}
