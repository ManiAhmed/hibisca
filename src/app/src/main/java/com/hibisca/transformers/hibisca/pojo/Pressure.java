package com.hibisca.transformers.hibisca.pojo;

/**
 * Class for storing information on the pressure readings.
 */

public class Pressure {
    private String plantID;
    private int pressure;

    public Pressure(String plantID, int pressure) {
        this.plantID = plantID;
        this.pressure = pressure;
    }

    /**
     * Gets the plant ID
     * @return the plant ID
     */
    public String getPlantID() { return plantID;}

    /**
     * Sets the plant ID
     * @param plantID the plant ID
     */
    public void setPlantID(String plantID) {
        this.plantID = plantID;
    }

    /**
     * Gets the pressure reading
     * @return the pressure reading
     */
    public int getPressure() {
        return pressure;
    }

    /**
     * Sets the pressure reading
     * @param pressure the pressure reading
     */
    public void setPressure(int pressure) {
        this.pressure = pressure;
    }

    public Pressure() {

    }
}
