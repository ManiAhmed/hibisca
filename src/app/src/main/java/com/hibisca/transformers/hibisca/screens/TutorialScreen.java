package com.hibisca.transformers.hibisca.screens;

import android.os.Bundle;

import com.hibisca.transformers.hibisca.MenuActivity;
import com.hibisca.transformers.hibisca.R;

public class TutorialScreen extends MenuActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial_screen);
        setTitle(R.string.titleTutorial);
    }
    @Override
    protected void onPause()
    {
        super.onPause();
        finish();
    }
}
