package com.hibisca.transformers.hibisca.database.controllers;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.hibisca.transformers.hibisca.database.DatabaseAccess;
import com.hibisca.transformers.hibisca.pojo.Humidity;
import java.util.ArrayList;
import java.util.List;

public class HumidityController
{
    /**
     *  A class for controlling interactions between an Android View (such as an activity/fragment),
     *  a model (that is, contact model) and an SQLite database. *
     * Created by Sleiman on 3/30/2018.
     */

    private DatabaseAccess databaseAccessHelper;
    private SQLiteDatabase sqLiteDatabase;
    private static final String TAG = HumidityController.class.getSimpleName();

    /**
     *  Creates a controller for controlling the interactions between a view and the humidity model.
     * @param context The application context that must be passed from an android view.
     */
    public HumidityController(Context context)
    {
        this.databaseAccessHelper = DatabaseAccess.getInstance(context);
    }


    public long insertHumidityReading(Humidity humidity)
    {
        //-- 1) Open the database.
        sqLiteDatabase = this.databaseAccessHelper.open();
        sqLiteDatabase.beginTransaction();
        long success = 0;
        try
        {
            ContentValues values = new ContentValues();
            values.put("PlantId", humidity.getPlantID());
            values.put("Humidity", humidity.getHumidity());
            success = sqLiteDatabase.insert("Humidity_Sensor", null, values);
            sqLiteDatabase.setTransactionSuccessful();
        }
        catch (Exception e)
        {
            Log.wtf(TAG, e.fillInStackTrace());
            Log.e("Database insert error", "could not insert");
        }
        finally
        {
            //-- 3) Very important, closeDatabase and unlock the database file.
            sqLiteDatabase.endTransaction();
            this.databaseAccessHelper.close();
            return success;
        }
    }

     /**
     * Reads a specific humidity value from the database.
     * @param plantID the id of the associated plant sensor data
     * @return a List of humidities
     */
    public List<Double> getHumidityReadings(String plantID)
    {
        sqLiteDatabase = this.databaseAccessHelper.open();
        sqLiteDatabase.beginTransaction();
        List<Double> list = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM Humidity_Sensor WHERE" + " PlantId" + "=\"" + plantID + "\";", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast())
        {
            list.add(cursor.getDouble(cursor.getColumnIndex("Humidity")));
            cursor.moveToNext();
        }
        cursor.close();
        sqLiteDatabase.endTransaction();
        this.databaseAccessHelper.close();
        return list;
    }

    /**
     * Deletes the specified humidity value from the database.
     * @param plntId the string that will be used to delete its corresponding
     * record from the database.
     */
    public void deleteHumidityReading(String plntId)
    {
        //-- 1) Open the database.
        sqLiteDatabase = this.databaseAccessHelper.open();
        try {
            sqLiteDatabase.execSQL("DELETE FROM " + "Humidity_Sensor" + " WHERE" + " PlantId" + "=\"" + plntId + "\";");
        } catch (Exception e) {
            Log.wtf(TAG, e.fillInStackTrace());
        } finally {
            sqLiteDatabase.close();
        }
    }
}
