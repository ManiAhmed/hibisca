package com.hibisca.transformers.hibisca.interfaces;

/**
 * Created by cstuser on 4/30/2018.
 */

/**
 * Passes the position in the playlist list that will allow the user
 * to play the specified playlist
 */
public interface OnPlaylistClicked {
    public void onPlaylistClicked(int position);
}
