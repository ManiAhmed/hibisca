package com.hibisca.transformers.hibisca.database.controllers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.hibisca.transformers.hibisca.database.DatabaseAccess;
import com.hibisca.transformers.hibisca.pojo.Pressure;

import java.util.ArrayList;
import java.util.List;

public class PressureController
{
    /**
     *  A class for controlling interactions between an Android View (such as an activity/fragment),
     *  a model (that is, contact model) and an SQLite database. *
     * Created by Sleiman on 3/30/2018.
     */

    private DatabaseAccess databaseAccessHelper;
    private SQLiteDatabase sqLiteDatabase;
    private static final String TAG = PressureController.class.getSimpleName();

    /**
     *  Creates a controller for controlling the interactions between a view and the Pressure model.
     * @param context The application context that must be passed from an android view.
     */
    public PressureController(Context context)
    {
        this.databaseAccessHelper = DatabaseAccess.getInstance(context);
    }


    public long insertPressureReading(Pressure pressure)
    {
        //-- 1) Open the database.
        sqLiteDatabase = this.databaseAccessHelper.open();
        sqLiteDatabase.beginTransaction();
        long success = 0;
        try
        {
            ContentValues values = new ContentValues();
            values.put("PlantId", pressure.getPlantID());
            values.put("Pressure_Reading", pressure.getPressure());
            success = sqLiteDatabase.insert("Pressure_Sensor", null, values);
            sqLiteDatabase.setTransactionSuccessful();
        }
        catch (Exception e)
        {
            Log.wtf(TAG, e.fillInStackTrace());
        }
        finally
        {
            //-- 3) Very important, closeDatabase and unlock the database file.
            sqLiteDatabase.endTransaction();
            this.databaseAccessHelper.close();
            return success;
        }
    }

    /**
     * Reads all pressure values from the database.
     * @return a List of alerts*/
    public List<Double> getPressureReadings(String plantID)
    {
        sqLiteDatabase = this.databaseAccessHelper.open();
        sqLiteDatabase.beginTransaction();
        List<Double> list = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM Pressure_Sensor WHERE" + " PlantId" + "=\"" + plantID + "\";", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast())
        {
            list.add(cursor.getDouble(cursor.getColumnIndex("Pressure_Reading")));
            cursor.moveToNext();
        }
        cursor.close();
        sqLiteDatabase.endTransaction();
        this.databaseAccessHelper.close();
        return list;
    }


    /**
     * Deletes the specified pressure reading from the database.
     * @param plntId the string that will be used to delete its corresponding
     * record from the database.
     */
    public void deletePressureReading(String plntId)
    {
        //-- 1) Open the database.
        sqLiteDatabase = this.databaseAccessHelper.open();
        try {
            sqLiteDatabase.execSQL("DELETE FROM " + "Pressure_Sensor" + " WHERE" + " PlantId" + "=\"" + plntId + "\";");
        } catch (Exception e) {
            Log.wtf(TAG, e.fillInStackTrace());
        } finally {
            sqLiteDatabase.close();
        }
    }
}
