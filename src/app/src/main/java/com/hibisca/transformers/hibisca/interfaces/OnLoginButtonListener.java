package com.hibisca.transformers.hibisca.interfaces;

import com.hibisca.transformers.hibisca.pojo.User;

/**
 * Created by harry on 2018-03-03.
 */

/**
 * Interface that creates an abstract method for when the user has completed their login data
 */
public interface OnLoginButtonListener {
    public void onFinishLogin(User user);
}
