package com.hibisca.transformers.hibisca.screens;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.SearchView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonParser;
import com.hibisca.transformers.hibisca.MenuActivity;
import com.hibisca.transformers.hibisca.R;
import com.hibisca.transformers.hibisca.adapters.NewsAdapter;
import com.hibisca.transformers.hibisca.pojo.NewsObject;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;


public class NewsScreen extends MenuActivity {
    private final String TAG = "NewsScreen";
    private SearchView searchView;
    private final String API_KEY = "73d4c1744fb74b12917134032eac81ea";
    private String newsResponse;
    private ArrayList<NewsObject> newsList;
    private String query;
    private boolean queryChanged;
    private NewsAdapter adapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_screen);

        recyclerView = (RecyclerView)findViewById(R.id.newsRecycler);
        newsList = new ArrayList<NewsObject>();
        //addDefault();

        searchView = (SearchView)findViewById(R.id.search);

        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String submission) {
                try {
                    query = submission;
                    Log.v(TAG, "query: " + query);
                    if (queryChanged) {
                        requestNews(query);
                        adapter.notifyDataSetChanged();
                    }
                    queryChanged = false;
                    adapter.notifyDataSetChanged();
                    return true;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String submission) {
                try {
                    queryChanged = true;
                    return queryChanged;
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            }
        });

        requestNews("plant");


    }

    public void requestNews(String queryString) {
        try {
            Log.v(TAG, "going to request something");
            RequestQueue queue = Volley.newRequestQueue(this);
            String url = "https://newsapi.org/v2/everything?q="+queryString+"&?sources=google-news&apiKey="+ API_KEY;
            StringRequest request = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    //textView.setText("Response is: " + response.substring(0,500));
                    newsResponse = response;
                    setArticles();
                    Log.v(TAG, "in onResponse");
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            //
                        }
                    });

            queue.add(request);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void setArticles() {
        Log.v(TAG, newsResponse);

        try {
            JSONObject jsonObject = new JSONObject(newsResponse);

            JSONArray jsonArray = jsonObject.getJSONArray("articles");

            NewsObject newsObject = null;
            JSONObject article =null;
            String rs = "";
            newsList.clear();
            for(int i = 0; i < jsonArray.length(); i++) {
                newsObject = new NewsObject();
                article = (JSONObject)jsonArray.get(i);

                newsObject.setTitle(article.getString("title"));
                newsObject.setAuthor(article.getString("author"));
                newsObject.setDescription(article.getString("description"));
                newsObject.setUrl(article.getString("url"));
                newsObject.setImageUrl(article.getString("urlToImage"));

                new ImageDownload().execute(newsObject.getImageUrl(), Integer.toString(i));

                JSONObject sourceObject = new JSONObject(article.getString("source"));
                newsObject.setSource(sourceObject.getString("name"));
                newsList.add(newsObject);

                Log.v(TAG, "result: " +  newsObject.getTitle());
                Log.v(TAG, "result: " +  newsObject.getAuthor());
                Log.v(TAG, "result: " +  newsObject.getUrl());
                Log.v(TAG, "result: " +  newsObject.getImageUrl());
                Log.v(TAG, "source: " +  ((sourceObject.getString("name"))));


                rs += newsObject.getTitle();
                rs += "\n" + newsObject.getAuthor();
                rs += "\n" + newsObject.getUrl();
                rs += "\n" + newsObject.getImageUrl();
                rs += "\n" + sourceObject.getString("name");
                rs += "\n\n";




            }
            displayFeed();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void displayFeed() {
        try {
            adapter = new NewsAdapter(newsList);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private class ImageDownload extends AsyncTask<String, Integer, Bitmap> {
        private int imageIndex;
        @Override
        protected Bitmap doInBackground(String... strings) {
            Bitmap bitmap = null;
            try {
                imageIndex = Integer.parseInt(strings[1]);
                //Declare URL connection
                URL url = new URL(strings[0]);
                HttpURLConnection connection = (HttpURLConnection)url.openConnection();

                //Open InputStream to connection
                connection.connect();
                InputStream stream = connection.getInputStream();

                //Download and decode the bitbmap using BitmapFactory
                bitmap = BitmapFactory.decodeStream(stream);
                stream.close();
                connection.disconnect();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            try {
                newsList.get(imageIndex).setNewsImage(bitmap);
                adapter.setImage(imageIndex, bitmap);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    private void addDefault() {
        NewsObject obj1 = new NewsObject();
        obj1.setSource("source");
        obj1.setDescription("descriprtion");
        obj1.setTitle("tittile");

        NewsObject obj2 = new NewsObject();
        obj2.setSource("source");
        obj2.setDescription("descriprtion");
        obj2.setTitle("tittile");

        NewsObject obj3 = new NewsObject();
        obj3.setSource("source");
        obj3.setDescription("descriprtion");
        obj3.setTitle("tittile");

        NewsObject obj4 = new NewsObject();
        obj4.setSource("source");
        obj4.setDescription("descriprtion");
        obj4.setTitle("tittile");
        newsList.add(obj1);
        newsList.add(obj2);
        newsList.add(obj3);
        newsList.add(obj4);
    }
}
