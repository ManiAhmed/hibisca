package com.hibisca.transformers.hibisca.interfaces;

import com.hibisca.transformers.hibisca.pojo.Plant;

/**
 * Created by cstuser on 4/17/2018.
 */

/**
 * Passes the pojo Plant to the myPlants Screen so that the plant can be added to the database
 *
 */
public interface addPlantFragmentListener
{
    public void addPlant(Plant plant);
}
