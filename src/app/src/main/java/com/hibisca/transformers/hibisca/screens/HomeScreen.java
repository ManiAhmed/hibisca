package com.hibisca.transformers.hibisca.screens;

import android.app.FragmentManager;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.hibisca.transformers.hibisca.database.controllers.ProfileController;
import com.hibisca.transformers.hibisca.interfaces.OnLoginButtonListener;
import com.hibisca.transformers.hibisca.pojo.User;
import com.hibisca.transformers.hibisca.pojo.UserSession;
import com.hibisca.transformers.hibisca.R;

/**
 * This screen/activity will give the user the choice
 * of signing up or login in if they already have.
 */
public class HomeScreen extends AppCompatActivity implements  OnLoginButtonListener
{

    ProfileController profileController;
    /**
     * Initializes the login button and sets the listener to display the
     * login fragment
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        setTitle("");
        Button lgn = (Button)findViewById(R.id.loginButton);
        profileController = new ProfileController(this);

        final MediaPlayer fwd = MediaPlayer.create(this, R.raw.forward);

        lgn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                showLoginScreen();
                fwd.start();
            }
        });
    }

    /**
     * Displays the login fragment that will pop up on the home screen
     */
    public void showLoginScreen()
    {
        FragmentManager fragmentManager = getFragmentManager();
        LoginScreen loginDialog = new LoginScreen();
        loginDialog.setCancelable(false);
        loginDialog.show(fragmentManager, "login Dialog");
    }

    public void onClickSignUp(View view) {
        Intent signUpIntent = new Intent(this, SignUpScreen.class);
        startActivity(signUpIntent);
        finish();
    }

    /**
     * Once user has finished login the database will make sure it is a
     * active account before allowing user to enter the app.
     * @param user = user trying to log in
     */
    @Override
    public void onFinishLogin(User user)
    {
        // Checks if username and password are part of the same account
        boolean isValid = profileController.authenticateUser(user);

        if(! isValid)
            Toast.makeText(this, UserSession.getInstance().getLoginErrMsg(), Toast.LENGTH_LONG).show();

        else
        {
            Intent accessGranted = new Intent(this, WelcomeScreen.class);
            startActivity(accessGranted);
            finish();
        }
    }
}
