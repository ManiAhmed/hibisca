package com.hibisca.transformers.hibisca.pojo;

/**
 * Class for storing information about humidity.
 */

public class Humidity {
    private String plantID;
    private double humidity;

    /**
     * Gets the plant ID
     * @return the plant ID
     */
    public String getPlantID() {
        return plantID;
    }

    /**
     * Sets the plant ID
     * @param plantID the plant ID
     */
    public void setPlantID(String plantID) {
        this.plantID = plantID;
    }

    /**
     * Gets the humidity
     * @return the humidity
     */
    public double getHumidity() {
        return humidity;
    }

    /**
     * Sets the humidity
     * @param humidity the humidity
     */
    public void setHumidity(double humidity) {
        this.humidity = humidity;
    }

    public Humidity(String plantID, double humidity) {
        this.plantID = plantID;
        this.humidity = humidity;
    }

    public Humidity() {

    }
}
