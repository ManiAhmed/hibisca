package com.hibisca.transformers.hibisca.database;

import android.content.Context;
import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;
/**
 *  A helper class for creating and accessing an SQLite database.
 */

public class DatabaseOpener extends SQLiteAssetHelper
{
    private static final String DATABASE_NAME = "hibiscus.db";
    private static final int DATABASE_VERSION = 1;

    /**
     * The arg-construct for the DatabaseOpener
     * @param context the context/activity/fragment etc. that will open the database.
     */
    public DatabaseOpener(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
}
