package com.hibisca.transformers.hibisca.pojo;

/**
 * Class for storing Plant information
 */
public class Plant {
    private String plantID;
    private String plantName;
    private String species;
    private String username;

    public Plant() {;}

    public Plant(String plantID, String plantName, String species, String username) {
        this.plantID = plantID;
        this.plantName = plantName;
        this.species = species;
        this.username = username;
    }
    public Plant(Plant plant) {
        this.plantID = plant.getPlantID();
        this.plantName = plant.getPlantName();
        this.species = plant.getSpecies();
        this.username = plant.getUsername();
    }

    /**
     * Gets the plant ID
     * @return the plant ID
     */
    public String getPlantID() {
        return plantID;
    }

    /**
     * Sets the plant ID
     * @param plantID the plant ID
     */
    public void setPlantID(String plantID) {
        this.plantID = plantID;
    }

    /**
     * Gets the plant name
     * @return the plant name
     */
    public String getPlantName() {
        return plantName;
    }

    /**
     * Sets the plant name
     * @param plantName the plant name
     */
    public void setPlantName(String plantName) {
        this.plantName = plantName;
    }

    /**
     * Gets the plant species
     * @return the plant species
     */
    public String getSpecies() {
        return species;
    }

    /**
     * Sets the plant species
     * @param species the plant species
     */
    public void setSpecies(String species) {
        this.species = species;
    }

    /**
     * Gets the username
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the username
     * @param username the username
     */
    public void setUsername(String username) {
        this.username = username;
    }
}
