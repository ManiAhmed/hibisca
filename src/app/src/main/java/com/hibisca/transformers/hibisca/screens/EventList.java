package com.hibisca.transformers.hibisca.screens;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.hibisca.transformers.hibisca.adapters.EventAdapter;

import com.hibisca.transformers.hibisca.R;
import com.hibisca.transformers.hibisca.database.controllers.AlertsController;

import java.util.ArrayList;
import java.util.List;

/**
 * This class will be used to display all the events
 * when the button at the bottom of the alert screen has been clicked.
 */
public class EventList extends AppCompatActivity
{

    private List<String> eventEntries =  new ArrayList<String>();
    private List<String> displayFormat =  new ArrayList<String>();
    private EventAdapter createList;
    private ListView lv;
    private AlertsController alrtCon;

    /**
     * Create the list view with the specific format.
     * @param savedInstanceState the current preferences of the activity
     *  in association with this fragment
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.event_list);
            alrtCon = new AlertsController(this);
            eventEntries = alrtCon.getAlerts();
            for(int i = 0; i < eventEntries.size(); i++)
            {
                String[] breakString = eventEntries.get(i).split(" ");
                String display = breakString[4] + "       " +breakString[1]+"/"+breakString[2]+"/"+breakString[3];
                displayFormat.add(display);
            }

            System.out.println(eventEntries);
            createList = new EventAdapter(this, R.layout.list_row_text,displayFormat);
            lv = findViewById(R.id.eventList);
            lv.setAdapter(createList);
            registerForContextMenu(lv);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }
    /**
     *  This will add a context menu to the list view
     *  that will enable the user to delete an event from the list view.
     * @param menu the container
     * @param view the view to add the context menu to.
     * @param menuInfo information about where the context menu appeared
     */

    @Override
    public void onCreateContextMenu(ContextMenu menu, View view, ContextMenu.ContextMenuInfo menuInfo)
    {
        super.onCreateContextMenu(menu,view,menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.delete_context,menu);
    }

    /**
     *  This method will handle deleting the alert/event from the database
     *   by using the given context menu.
     * @param item the item that was slected from the list view.
     * @return true indication the item was selected/handled.
     */
    @Override
    public boolean onContextItemSelected(MenuItem item)
    {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.deleteStock:
                String[] deleteKey = displayFormat.get(info.position).split("       ");
                System.out.println(deleteKey[0]);
                alrtCon.deleteAlert(deleteKey[0]);
                displayFormat.remove(info.position);
                createList.notifyDataSetChanged();
                Toast.makeText(getBaseContext(), R.string.txtRemoveEvent, Toast.LENGTH_LONG).show();
                return true;
            case R.id.checkEvent:
                String[] split = displayFormat.get(info.position).split("       ");
                String str = alrtCon.getAlert(split[0]);
                AlertDialog.Builder eventDialog = new AlertDialog.Builder(this);
                eventDialog.setTitle(split[0]);
                eventDialog.setMessage(str);
                eventDialog.setNegativeButton(R.string.cancel,new DialogInterface.OnClickListener()
                {
                    /**
                     * Return to the alertscreen/calendar
                     * @param dialog the dialog being inflated
                     * @param id the id of the positive button inflated into the dialog
                     */
                    public void onClick(DialogInterface dialog,int id)
                    {
                        dialog.cancel();
                    }
                });
                eventDialog.show();
                return true;
            default:
                return super.onContextItemSelected(item);

        }
    }
}
