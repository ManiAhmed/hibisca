package com.hibisca.transformers.hibisca.fragments;

import android.app.DialogFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.hibisca.transformers.hibisca.interfaces.OnUpdateButtonListener;
import com.hibisca.transformers.hibisca.pojo.User;
import com.hibisca.transformers.hibisca.pojo.UserSession;
import com.hibisca.transformers.hibisca.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Dialog Fragment that allows the user to update their profile with new info
 */
public class UpdateUserFragment extends DialogFragment {

    private static final String EMAIL_REGEX = "^\\w+@\\w+\\.[a-z]{2,3}$";
    private static final String POSTALCODE_REGEX = "^[A-Z]\\d[A-Z]\\s\\d[A-Z]\\d$";
    TextView username;
    EditText editEmail;
    EditText editPostalCode;
    EditText editPassword;
    EditText editConfirmPassword;
    Button btnUpdate;
    Button btnCancel;
    private Pattern emailPattern;
    private Matcher emailMatcher;
    private Pattern postalCodePattern;
    private Matcher postalCodeMatcher;
    User updatedUser;

    /**
     * Default Constructor
     */
    public UpdateUserFragment(){

    }

    /**
     * Initializes all the variables as well as the listeners for the ok and cancel buttons
     * Returns the login fragment for the user to interact with as checks to make sure the form
     * validation has been followed correctly
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View updateUser = inflater.inflate(R.layout.activity_update_user_fragment, container, false);

        updatedUser = new User();
        username = updateUser.findViewById(R.id.updateTitle);
        username.setText(UserSession.getInstance().getUsername());
        editEmail = updateUser.findViewById(R.id.updateEmail);
        Log.d("Update", editEmail.getText().toString());
        editPostalCode = updateUser.findViewById(R.id.updatePostalCode);
        editPassword = updateUser.findViewById(R.id.updatePassword);
        editConfirmPassword = updateUser.findViewById(R.id.updateConfirmPassword);
        btnUpdate = updateUser.findViewById(R.id.updateUser);
        btnCancel = updateUser.findViewById(R.id.cancelUpdate);

        emailPattern = Pattern.compile(EMAIL_REGEX);
        emailMatcher = emailPattern.matcher(editEmail.getText().toString());

        postalCodePattern = Pattern.compile(POSTALCODE_REGEX);
        postalCodeMatcher = postalCodePattern.matcher(editPostalCode.getText().toString());


        btnUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = editEmail.getText().toString();
                String postalCode = editPostalCode.getText().toString();
                String password = editPassword.getText().toString();
                String confirmPassword = editConfirmPassword.getText().toString();

                emailPattern = Pattern.compile(EMAIL_REGEX);
                emailMatcher = emailPattern.matcher(email);

                postalCodePattern = Pattern.compile(POSTALCODE_REGEX);
                postalCodeMatcher = postalCodePattern.matcher(postalCode);

                 if(! emailMatcher.matches())
                    Toast.makeText(getActivity(), R.string.errEmail, Toast.LENGTH_LONG).show();

                 else if(! postalCodeMatcher.matches())
                     Toast.makeText(getActivity(), R.string.errPostalCode, Toast.LENGTH_LONG).show();

                 else if(! password.equals(confirmPassword))
                     Toast.makeText(getActivity(), R.string.errSamePassword, Toast.LENGTH_LONG).show();

                 else if(password.length() < 6 || password.length() > 12)
                     Toast.makeText(getActivity(), R.string.errPasswordLength, Toast.LENGTH_LONG).show();

                 else {
                     updatedUser.setUsername(UserSession.getInstance().getUsername());
                     updatedUser.setFirstName(UserSession.getInstance().getFirstName());
                     updatedUser.setLastName(UserSession.getInstance().getLastName());
                     updatedUser.setEmail(editEmail.getText().toString());
                     updatedUser.setPostalCode(editPostalCode.getText().toString());
                     updatedUser.setPassword(editPassword.getText().toString());

                     OnUpdateButtonListener updateButtonListener = (OnUpdateButtonListener) getActivity();
                     updateButtonListener.onFinishUpdate(updatedUser);
                 }

                dismiss();
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                dismiss();
            }
        });

        return updateUser;
    }
}
