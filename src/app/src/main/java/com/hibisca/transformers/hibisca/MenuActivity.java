package com.hibisca.transformers.hibisca;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.hibisca.transformers.hibisca.pojo.UserSession;
import com.hibisca.transformers.hibisca.screens.AlertScreen;
import com.hibisca.transformers.hibisca.screens.GraphScreen;
import com.hibisca.transformers.hibisca.screens.HumidityScreen;
import com.hibisca.transformers.hibisca.screens.LightScreen;
import com.hibisca.transformers.hibisca.screens.MyPlantsScreen;
import com.hibisca.transformers.hibisca.screens.NewsScreen;
import com.hibisca.transformers.hibisca.screens.PlantLocationsScreen;
import com.hibisca.transformers.hibisca.screens.PlaylistScreen;
import com.hibisca.transformers.hibisca.screens.PressureScreen;
import com.hibisca.transformers.hibisca.screens.ProfileScreen;
import com.hibisca.transformers.hibisca.screens.TemperatureScreen;
import com.hibisca.transformers.hibisca.screens.TipsScreen;
import com.hibisca.transformers.hibisca.screens.TutorialScreen;

/**
 * This class is used by all other activities
 * that want to include a menu on their activity screen
 * to navigate through the application.
 * To do reuse code, we made sure that any screen
 * using the drawer menu format extends this class.
 */
public class MenuActivity extends AppCompatActivity
{
    private DrawerLayout dl;
    TextView name;

    /**
     * Instantiates this class on any activity it extends.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    /**
     * Overrides the current view of any activities layout to provide the menu
     * and reuse the code when and where it it is required. It attaches all necessary
     * menu items in to drawer to the necessary listeners to allow the user
     * to navigate through the application and then also adds the itself to the acitivity
     * it extends with the appropriate Action bar.
     * @param layoutResID
     */
    @Override
    public void setContentView(int layoutResID)
    {
        dl = (DrawerLayout) getLayoutInflater().inflate(R.layout.drawer_main, null);
        dl.setScrimColor(0x70ffff00);
        FrameLayout activityContainer = (FrameLayout) dl.findViewById(R.id.frmlyt);
        getLayoutInflater().inflate(layoutResID, activityContainer, true);
        super.setContentView(dl);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
        NavigationView navigationView = findViewById(R.id.nav_view);
        View navHeader = navigationView.getHeaderView(0);
        name = navHeader.findViewById(R.id.txtPlaceholderName);
        name.setText(UserSession.getInstance().getUsername());
        name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent goToProfile = new Intent(getBaseContext(), ProfileScreen.class);
                startActivity(goToProfile);
            }
        });

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener()
        {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item)
            {
                item.setChecked(true);
                int id = item.getItemId();
                switch(id)
                {
                    case R.id.nav_Profile:
                        dl.closeDrawers();
                        Intent navProfile = new Intent(getBaseContext(), ProfileScreen.class);
                        startActivity(navProfile);
                        break;
                    case R.id.nav_Plants:
                        dl.closeDrawers();
                        Intent navPlants = new Intent(getBaseContext(), MyPlantsScreen.class);
                        startActivity(navPlants);
                        break;
                    case R.id.nav_Temp:
                        dl.closeDrawers();
                        Intent navTemp = new Intent(getBaseContext(), TemperatureScreen.class);
                        startActivity(navTemp);
                        break;
                    case R.id.nav_Humi:
                        dl.closeDrawers();
                        Intent navHumi = new Intent(getBaseContext(), HumidityScreen.class);
                        startActivity(navHumi);
                        break;
                    case R.id.nav_Light:
                        dl.closeDrawers();
                        Intent navLight = new Intent(getBaseContext(), LightScreen.class);
                        startActivity(navLight);
                        break;
                    case R.id.nav_Press:
                        dl.closeDrawers();
                        Intent navPress = new Intent(getBaseContext(), PressureScreen.class);
                        startActivity(navPress);
                        break;
                    case R.id.nav_Playlists:
                        dl.closeDrawers();
                        Intent navPlay = new Intent(getBaseContext(), PlaylistScreen.class);
                        startActivity(navPlay);
                        break;
                    case R.id.nav_Area:
                        dl.closeDrawers();
                        Intent navMap = new Intent(getBaseContext(), PlantLocationsScreen.class);
                        startActivity(navMap);
                        break;
                    case R.id.nav_News:
                        dl.closeDrawers();
                        Intent navNews = new Intent(getBaseContext(), NewsScreen.class);
                        startActivity(navNews);
                        break;
                    case R.id.nav_Alerts:
                        dl.closeDrawers();
                        Intent navAlerts = new Intent(getBaseContext(), AlertScreen.class);
                        startActivity(navAlerts);
                        break;

                    case R.id.nav_Graph:
                        dl.closeDrawers();
                        Intent navGraph = new Intent(getBaseContext(), GraphScreen.class);
                        startActivity(navGraph);
                        break;

                    case R.id.nav_Tips:
                        dl.closeDrawers();
                        Intent navTip = new Intent(getBaseContext(), TipsScreen.class);
                        startActivity(navTip);
                        break;
                    case R.id.nav_Tutorial:
                        dl.closeDrawers();
                        Intent navTut = new Intent(getBaseContext(), TutorialScreen.class);
                        startActivity(navTut);
                        break;
                }
                return true;
            }
        });
    }

    /**
     * Creates the hamburger icon to be clicked and reveals
     * the drawer items in the menu.
     * @param item The item selected from the drawer menu
     * @return True if an item was select and false otherwise.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                dl.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}