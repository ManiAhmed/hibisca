package com.hibisca.transformers.hibisca.interfaces;

import com.hibisca.transformers.hibisca.pojo.User;

/**
 * Created by cstuser on 4/17/2018.
 */

/**
 * Interface that creates an abstract method for when the user has completed their update data
 */
public interface OnUpdateButtonListener {
    public void onFinishUpdate(User user);
}
