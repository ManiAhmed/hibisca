package com.hibisca.transformers.hibisca.database;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * A class/helper used to open and access the database associated with this api.
 */

public class DatabaseAccess
{
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static DatabaseAccess instance;


    /**
     * Private constructor to aboid object creation from outside classes.
     *
     * @param context
     */
    private DatabaseAccess(Context context)
    {
        this.openHelper = new com.hibisca.transformers.hibisca.database.DatabaseOpener(context);
    }

    /**
     * Return a singleton instance of DatabaseAccess.
     *
     * @param context the Context
     * @return the instance of DabaseAccess
     */
    public static DatabaseAccess getInstance(Context context)
    {
        if (instance == null)
        {
            instance = new DatabaseAccess(context);
        }
        return instance;
    }

    /**
     * Open the database connection.
     */
    public SQLiteDatabase open()
    {
        this.database = openHelper.getWritableDatabase();
        return this.database;
    }

    /**
     * Close the database connection.
     */
    public void close()
    {
        if (database != null)
        {
            this.database.close();
        }
    }
}