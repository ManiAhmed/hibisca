package com.hibisca.transformers.hibisca.database.controllers;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


import com.hibisca.transformers.hibisca.database.DatabaseAccess;

import com.hibisca.transformers.hibisca.pojo.PlantInfo;

import java.util.ArrayList;
import java.util.List;

public class PlantInfoController
{
    /**
     *  A class for controlling interactions between an Android View (such as an activity/fragment),
     *  a model (that is, contact model) and an SQLite database. *
     * Created by Sleiman on 3/30/2018.
     */

    private DatabaseAccess databaseAccessHelper;
    private SQLiteDatabase sqLiteDatabase;
    private static final String TAG = PlantInfoController.class.getSimpleName();

    /**
     *  Creates a controller for controlling the interactions between a view and the plant_info model.
     * @param context The application context that must be passed from an android view.
     */
    public PlantInfoController(Context context)
    {
        this.databaseAccessHelper = DatabaseAccess.getInstance(context);
    }


    public long insertPlantInfo(PlantInfo plant)
    {
        //-- 1) Open the database.
        sqLiteDatabase = this.databaseAccessHelper.open();
        sqLiteDatabase.beginTransaction();
        long success = 0;
        try
        {
            ContentValues values = new ContentValues();
            values.put("Species", plant.getSpecies());
            values.put("Fertilizer", plant.getFertilizer());
            values.put("Light_Req", plant.getLight());
            values.put("Temp_Req", plant.getTemp());
            values.put("Humidity_Req", plant.getHumidity());
            success = sqLiteDatabase.insert("Plant_Info", null, values);
            sqLiteDatabase.setTransactionSuccessful();
        }
        catch (Exception e)
        {
            Log.wtf(TAG, e.fillInStackTrace());
        }
        finally
        {
            //-- 3) Very important, closeDatabase and unlock the database file.
            sqLiteDatabase.endTransaction();
            this.databaseAccessHelper.close();
            return success;
        }
    }

    public PlantInfo getPlantInformation(String species) {
        sqLiteDatabase = this.databaseAccessHelper.open();
        sqLiteDatabase.beginTransaction();

        String[] whereArgs = new String[] {species};

        PlantInfo plant = new PlantInfo();
        Cursor cursor = sqLiteDatabase.query("Plant_Info", null, "Species = ?", whereArgs, null, null, null);
        cursor.moveToFirst();

        while(!cursor.isAfterLast()) {
            plant.setSpecies(cursor.getString(0));
            plant.setFertilizer(cursor.getString(1));
            plant.setLight(cursor.getInt(2));
            plant.setTemp(cursor.getInt(3));
            plant.setHumidity(cursor.getInt(4));
            cursor.moveToNext();
        }

        cursor.close();
        sqLiteDatabase.endTransaction();
        this.databaseAccessHelper.close();
        return plant;
    }

    /**
     * Reads all plant information from the database.
     * @return a List of alerts
     */
    public List<PlantInfo> getAllPlantsInformation()
    {
        sqLiteDatabase = this.databaseAccessHelper.open();
        sqLiteDatabase.beginTransaction();
        List<PlantInfo> list = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM Plant_Info", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast())
        {
            PlantInfo plant = new PlantInfo();

            plant.setSpecies(cursor.getString(0));
            plant.setFertilizer(cursor.getString(1));
            plant.setLight(cursor.getInt(2));
            plant.setTemp(cursor.getInt(3));
            plant.setHumidity(cursor.getInt(4));

            list.add(plant);

            cursor.moveToNext();
        }

        cursor.close();
        sqLiteDatabase.endTransaction();
        this.databaseAccessHelper.close();
        return list;
    }

    /**
     * Deletes the specified plant information from the database.
     * @param spcies the string that will be used to delete its corresponding
     * record from the database.
     */
    public void deletePlantInfo(String spcies)
    {
        //-- 1) Open the database.
        sqLiteDatabase = this.databaseAccessHelper.open();
        try {
            sqLiteDatabase.execSQL("DELETE FROM " + "Plant_Info" + " WHERE" + " Species" + "=\"" + spcies + "\";");
        } catch (Exception e) {
            Log.wtf(TAG, e.fillInStackTrace());
        } finally {
            sqLiteDatabase.close();
        }
    }
}
