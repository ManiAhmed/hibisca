package com.hibisca.transformers.hibisca.screens;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.hardware.SensorEventListener;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hibisca.transformers.hibisca.database.controllers.LightController;
import com.hibisca.transformers.hibisca.fragments.SelectPlantFragment;
import com.hibisca.transformers.hibisca.MenuActivity;
import com.hibisca.transformers.hibisca.pojo.Light;
import com.hibisca.transformers.hibisca.pojo.Plant;
import com.hibisca.transformers.hibisca.MenuActivity;
import com.hibisca.transformers.hibisca.pojo.Light;
import com.hibisca.transformers.hibisca.R;

/**
 * This screen shows the user the light levels the sensors their device will read around them
 */

public class LightScreen extends MenuActivity implements SensorEventListener, SelectPlantFragment.SelectPlantListener {
    private SensorManager lightSensorManager;
    private LightController controller;
    private Sensor lightSensor;
    private TextView displayLight;
    private Button getLight;
    private ImageView lightImg;
    private double lightReading;
    private Button readForPlant;
    private Plant selectedPlant = null;
    private Button deleteReading;

    /**
     * Initialization of variable, layout and onClick listeners to activate the sensor
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_light_screen);
        setTitle(R.string.titleLighting);
        controller = new LightController(this);
        lightSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        lightSensor = lightSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
        readForPlant = (Button) findViewById(R.id.btnReadForPlant);
        deleteReading = (Button)findViewById(R.id.deleteReading);

        getLight = (Button)findViewById(R.id.btnLightReading);
        displayLight = (TextView)findViewById(R.id.lightValue);
        lightImg = (ImageView)findViewById(R.id.light);

        getLight.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                getLight.setText(R.string.btnStopReading);
                lightSensorManager.registerListener(LightScreen.this, lightSensor, SensorManager.SENSOR_DELAY_NORMAL);
            }
        });

        getLight.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                getLight.setText(R.string.btnBeginReading);
                lightSensorManager.unregisterListener(LightScreen.this, lightSensor);
                if (selectedPlant != null)
                    storeValue(lightReading);
                return true;
            }
        });

        readForPlant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager manager = getSupportFragmentManager();
                SelectPlantFragment selectPlant = new SelectPlantFragment();
                selectPlant.setCancelable(true);
                selectPlant.show(manager, "lighting");
            }
        });

        deleteReading.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteLightReading();
            }
        });
    }

    /**
     * Retreives the selected plant
     * @param plant the plant the user selected
     */
    public void selectPlant(Plant plant) {
        Log.v("SELECTED", "SEL: " + plant.getPlantName());
        selectedPlant = new Plant(plant);
    }

    public boolean storeValue(double lightReading) {
        Light light = new Light(selectedPlant.getPlantID(), lightReading);
        LightController controller = new LightController(this);
        controller.insertLightReading(light);

        return true;
    }

    /**
     * Removes a light reading from the database
     */
    public void deleteLightReading() {
        if(selectedPlant == null)
            Toast.makeText(this, R.string.selectPlantToDelete, Toast.LENGTH_SHORT).show();
        else {
            controller.deleteLightReading(selectedPlant.getPlantID());
        }
    }

    /**
     * Displays the light levels to the user by lux and by adjusting the image @lightImg
     * @param event the SensorEvent containing the light sensor data
     */
    public final void onSensorChanged(SensorEvent event) {
        lightReading = event.values[0];
        if(event.values[0] < 8000) {
            displayLight.setText(Float.toString(event.values[0]) + " lx");
            lightImg.setImageAlpha(50);
        }

        else if(event.values[0] >= 8000 && event.values[0] < 16000) {
            displayLight.setText(Float.toString(event.values[0]) + " lx");
            lightImg.setImageAlpha(100);
        }

        else if(event.values[0] >= 16000 && event.values[0] < 24000) {
            displayLight.setText(Float.toString(event.values[0]) + " lx");
            lightImg.setImageAlpha(150);
        }

        else if(event.values[0] >= 24000 && event.values[0] < 32000) {
            displayLight.setText(Float.toString(event.values[0]) + " lx");
            lightImg.setImageAlpha(200);
        }

        else if(event.values[0] >= 32000 && event.values[0] <= 40000) {
            displayLight.setText(Float.toString(event.values[0]) + " lx");
            lightImg.setImageAlpha(255);
        }
    }

    /**
     * Called when the accuracy of the sensor is change (not implemented)
     * @param sensor
     * @param i
     */
    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
        ;
    }

    /**
     * When paused the light sensor manager stops taking in readings.
     */
    @Override
    protected void onPause()
    {
        super.onPause();
    }
}
