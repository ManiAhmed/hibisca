package com.hibisca.transformers.hibisca.pojo;

public class PlantInfo {
    private String species;
    private String fertilizer;
    private int light;
    private int temp;

    public String getSpecies() {
        return species;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public String getFertilizer() {
        return fertilizer;
    }

    public void setFertilizer(String fertilizer) {
        this.fertilizer = fertilizer;
    }

    public int getLight() {
        return light;
    }

    public void setLight(int light) {
        this.light = light;
    }

    public int getTemp() {
        return temp;
    }

    public void setTemp(int temp) {
        this.temp = temp;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public PlantInfo(String species, String fertilizer, int light, int temp, int humidity) {

        this.species = species;
        this.fertilizer = fertilizer;
        this.light = light;
        this.temp = temp;
        this.humidity = humidity;
    }

    private int humidity;

    public PlantInfo() {
    }
}
