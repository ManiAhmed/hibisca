package com.hibisca.transformers.hibisca.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import com.hibisca.transformers.hibisca.interfaces.addPlantFragmentListener;
import com.hibisca.transformers.hibisca.pojo.Plant;
import com.hibisca.transformers.hibisca.pojo.UserSession;
import com.hibisca.transformers.hibisca.R;

/**
 * Created by cstuser on 4/17/2018.
 */

public class AddPlantFragment extends DialogFragment {
    View view;

    /**
     * Creates the input dialog for the user to add a plant to the db
     * @param savedInstanceState
     * @return
     */
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final addPlantFragmentListener listener = (addPlantFragmentListener) getActivity();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        view = inflater.inflate(R.layout.add_plant_dialog_box_layout, null);
        builder.setView(view);

        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {

                EditText plantID = (EditText) view.findViewById(R.id.editTextPlantID);
                String plantIDString = (plantID.getText().toString());

                EditText plantName = (EditText) view.findViewById(R.id.editTextPlantName);
                String plantNameString = (plantName.getText().toString());

                EditText plantSpecies = (EditText) view.findViewById(R.id.editTextPlantSpecies);
                String plantSpeciesString = (plantSpecies.getText().toString());

                String username;

                if(UserSession.getInstance().getUsername() == null) {
                    username = "none";
                }

                else {
                    username = UserSession.getInstance().getUsername();
                }

                Plant plant = new Plant(plantIDString, plantNameString, plantSpeciesString, username);
                listener.addPlant(plant);

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                AddPlantFragment.this.getDialog().cancel();
            }
        });

        return builder.create();
    }
}
