package com.hibisca.transformers.hibisca.checkconnect;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 *  This class will be used to verify network connectivity
 *  before making any api or network calls.
 */
public class IConnect
{
    private Context appContext;

    public IConnect(Context context)
    {
        this.appContext = context;
    }

    /**
     * Checks whether or not there is an internet connection
     * @return true if connected to the internet
     */
    public boolean checkForConnection()
    {
        boolean isConnected = false;

        ConnectivityManager cm = (ConnectivityManager)appContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if(activeNetwork != null && activeNetwork.isConnectedOrConnecting())
        {
            isConnected = true;
        }
        return isConnected;
    }
}
