package com.hibisca.transformers.hibisca.pojo;

/**
 * Class for storing information about the light readings.
 */

public class Light {
    private String plantID;
    private double light;

    /**
     * Gets the plant ID
     * @return the plant ID
     */
    public String getPlantID() {
        return plantID;
    }

    /**
     * Sets the plant ID
     * @param plantID the plant ID
     */
    public void setPlantID(String plantID) {
        this.plantID = plantID;
    }

    /**
     * Gets the light reading
     * @return the light reading
     */
    public double getLight() {
        return light;
    }

    /**
     * Sets the light value
     * @param light the light value
     */
    public void setLight(double light) {
        this.light = light;
    }

    public Light(String plantID, double light) {
        this.plantID = plantID;
        this.light = light;
    }

    public Light() {

    }
}
