package com.hibisca.transformers.hibisca.database.controllers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.hibisca.transformers.hibisca.database.DatabaseAccess;
import com.hibisca.transformers.hibisca.pojo.User;
import com.hibisca.transformers.hibisca.pojo.UserSession;

import java.util.ArrayList;
import java.util.List;

public class ProfileController
{
    /**
     *  A class for controlling interactions between an Android View (such as an activity/fragment),
     *  a model (that is, contact model) and an SQLite database. *
     * Created by Sleiman on 3/30/2018.
     */

    private DatabaseAccess databaseAccessHelper;
    private SQLiteDatabase sqLiteDatabase;
    private static final String TAG = ProfileController.class.getSimpleName();

    /**
     *  Creates a controller for controlling the interactions between a view and the profile model.
     * @param context The application context that must be passed from an android view.
     */
    public ProfileController(Context context)
    {
        this.databaseAccessHelper = DatabaseAccess.getInstance(context);
    }

    /**
     * Checks to see if there are any users in the User table that have the same name as the user that
     * is signing up. If true, the method returns true. If false, it returns false and sets the sign up
     * error message for the user to see.
     * @param user = the newly signed up user
     * @return isUnique
     */
    public boolean isUserUnique(User user){
        boolean isUnique = false;
        sqLiteDatabase = this.databaseAccessHelper.open();
        try{
            String[] columns = {"username"};
            Cursor cursor = sqLiteDatabase.query("Profile_Information", columns, "username = ?", new String[] {user.getUsername()},
                    null, null, null);
            if(cursor != null) {
                if (cursor.getCount() == 0)
                    isUnique = true;

                else
                    UserSession.getInstance().setSignUpErrMsg("Username is already taken");
            }
        }
        catch (Exception e) {
            Log.wtf(TAG, e.fillInStackTrace());
        } finally {
            databaseAccessHelper.close();
        }

        return isUnique;
    }

    /**
     * Adds a newly created user into the database
     * @param newUser
     */
    public void insertProfile(User newUser)
    {
        sqLiteDatabase = this.databaseAccessHelper.open();
        sqLiteDatabase.beginTransaction();
        try
        {
            ContentValues values = new ContentValues();
            values.put("Username", newUser.getUsername());
            values.put("First_Name", newUser.getFirstName());
            values.put("Last_Name", newUser.getLastName());
            values.put("PostalCode", newUser.getPostalCode());
            values.put("Email", newUser.getEmail());
            values.put("Password", newUser.getPassword());
            sqLiteDatabase.insert("Profile_Information", null, values);
            sqLiteDatabase.setTransactionSuccessful();
        }
        catch (Exception e)
        {
            Log.wtf(TAG, e.fillInStackTrace());
        }
        finally
        {
            sqLiteDatabase.endTransaction();
            this.databaseAccessHelper.close();
        }
    }

    /**
     * Reads all profiles from the database.
     * @return a List of alerts
     */
    public List<String> getProfiles()
    {
        sqLiteDatabase = this.databaseAccessHelper.open();
        sqLiteDatabase.beginTransaction();
        List<String> list = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM Profile_Information", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast())
        {
            list.add(cursor.getString(0));
            cursor.moveToNext();
        }
        cursor.close();
        sqLiteDatabase.endTransaction();
        this.databaseAccessHelper.close();
        return list;
    }

    /**
     * Deletes the specified profile from the database.
     * @param prflId the string that will be used to delete its corresponding
     * record from the database.
     */
    public void deleteAlert(String prflId)
    {
        sqLiteDatabase = this.databaseAccessHelper.open();
        try {
            sqLiteDatabase.execSQL("DELETE FROM " + "Profile_Information" + " WHERE" + " ProfileId" + "=\"" + prflId + "\";");
        } catch (Exception e) {
            Log.wtf(TAG, e.fillInStackTrace());
        } finally {
            sqLiteDatabase.close();
        }
    }

    /**
     * Checks if the user is already in database when they try to
     * log in
     * @param user
     * @return true if user is in database else return false
     */
    public boolean authenticateUser(User user) {
        boolean isValid = false;
        UserSession session = UserSession.getInstance();
        sqLiteDatabase = this.databaseAccessHelper.open();
        try {
            String[] columns = {"username", "first_name", "last_name", "postalcode", "email", "password"};

            Cursor cursor = sqLiteDatabase.query("Profile_Information", columns, "username = ? AND password = ?", new String[]{user.getUsername(), user.getPassword()},
                    null, null, null);
            if (cursor != null) {
                //-- Number of matched users.
                if (cursor.getCount() == 1) {
                    isValid = true;
                    //-- Keep track of the currently logged user's info.
                    cursor.moveToFirst();
                    session.setUsername(cursor.getString(0));
                    Log.d(TAG, session.getUsername());
                    session.setFirstName(cursor.getString(1));
                    Log.d(TAG, session.getFirstName());
                    session.setLastName(cursor.getString(2));
                    session.setPostalCode(cursor.getString(3));
                    session.setEmail(cursor.getString(4));
                    session.setPassword(cursor.getString(5));
                } else {
                    //-- Unsuccessful login.
                    UserSession error = UserSession.getInstance();
                    error.setLoginErrMsg("Failed to login... change me!");

                }
            }
        } catch (Exception e) {
            Log.wtf(TAG, e.fillInStackTrace());
        } finally {
            sqLiteDatabase.close();
        }
        UserSession.getInstance().setLoggedIn(isValid);
        return isValid;
    }

    /**
     * Updates the user's profile given their username
     * @param oldUser
     * @param newUser
     */
    public void updateUser(User oldUser, User newUser) {
        sqLiteDatabase = this.databaseAccessHelper.open();
        try {
            ContentValues values = new ContentValues();
            values.put("username", newUser.getUsername());
            values.put("first_name", newUser.getFirstName());
            values.put("last_name", newUser.getLastName());
            values.put("email", newUser.getEmail());
            values.put("postalcode", newUser.getPostalCode());
            values.put("password", newUser.getPassword());
            sqLiteDatabase.update("Profile_Information", values, "username = ?", new String[]{oldUser.getUsername()});
        } catch (Exception e) {
            Log.wtf(TAG, e.fillInStackTrace());
        } finally {
            sqLiteDatabase.close();
        }
    }
}
