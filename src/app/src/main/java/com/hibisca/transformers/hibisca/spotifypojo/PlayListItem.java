package com.hibisca.transformers.hibisca.spotifypojo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by cstuser on 4/30/2018.
 */

/**
 * POJO class for the spotify playlist, it holds the parsed json object
 * into this class and allows the json to be parsed into an array and a
 * single object
 */
public class PlayListItem {
    private String id;
    private String name;

    /**
     * Default Constructor
     */
    public PlayListItem() {

    }

    /**
     * Gets id of the playlist
     * @return id of the playlist
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id of the playlist
     * @param id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets name of the playlist
     * @return name of the playlist
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name of the playlist
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "PlayListItem{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }

    /**
     * Converts the JSONObject into the PlayListItem object
     * @param jsonObject
     * @return
     * @throws JSONException
     */
    public static PlayListItem fromJson(JSONObject jsonObject) throws JSONException {
        PlayListItem playlist = new PlayListItem();
        playlist.id = jsonObject.getString("id");
        playlist.name = jsonObject.getString("name");


        return playlist;
    }

    /**
     * Converts the jsonArray of PlayListItem into the PlayListItem object
     * @param jsonArray
     * @return
     */
    public static ArrayList<PlayListItem> fromJson(JSONArray jsonArray){
        JSONObject playlistJson;
        ArrayList<PlayListItem> items = new ArrayList<PlayListItem>(jsonArray.length());

        for(int i = 0; i < jsonArray.length(); ++i)
        {
            try {
                playlistJson = jsonArray.getJSONObject(i);
                PlayListItem item = PlayListItem.fromJson(playlistJson);
                if(item != null){
                    items.add(item);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return items;
    }
}
