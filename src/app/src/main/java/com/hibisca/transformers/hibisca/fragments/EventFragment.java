package com.hibisca.transformers.hibisca.fragments;

import android.app.DialogFragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.Toast;

import com.hibisca.transformers.hibisca.R;

/**
 * This class will be used to add an event/alert to any date selected by the user
 */
public class EventFragment extends DialogFragment
{
    public EventFragment(){};

    /**
     * An interface used to pass data(the alertId and description) back to the activity (AlertScreen)
     */
    public interface OnFragmentButtonListener
    {
        void onFragmentButtonClicked(String evntID, String evntDesc);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.add_event_layout, container, false);
        String id = "";
        String desc = "";
        final EditText alrtId = (EditText)view.findViewById(R.id.eventId);
        final EditText alrtDesc = (EditText)view.findViewById(R.id.eventDesc);
        Button ok = (Button)view.findViewById(R.id.okEventFrag);
        Button cancel = (Button)view.findViewById(R.id.cancelEventFrag);
        final CalendarView selectDate = (CalendarView)container.findViewById(R.id.alrtCalendar);
        final Button viewAll = (Button)container.findViewById(R.id.viewAll);


        cancel.setOnClickListener(new View.OnClickListener()
        {
            /**
             *  Cancels the users request to add an event date
             *  and fixes the activity's view so that the user
             *  may once again see the calendar and view all button.
             * @param view the button that was pressed in the fragment.
             */
            @Override
            public void onClick(View view)
            {
                dismiss();
                selectDate.setVisibility(View.VISIBLE);
                viewAll.setVisibility(View.VISIBLE);
            }
        });

        ok.setOnClickListener(new View.OnClickListener()
        {
            /**
             * This will tell the activity to add the event
             * with the given information as long as both fields
             * contain data. This method does not check for uniqueness.
             * Uniqueness is handled by the activity once the data has been passed back.
             * @param view the button that was clicked.
             */
        @Override
        public void onClick(View view)
        {
            OnFragmentButtonListener activity = (OnFragmentButtonListener)getActivity();
            if(!alrtId.getText().toString().isEmpty() && !alrtDesc.getText().toString().isEmpty())//Validation so that no null values are stored or retrieved by the database
            {
                activity.onFragmentButtonClicked(alrtId.getText().toString(),alrtDesc.getText().toString());
                dismiss();
                selectDate.setVisibility(View.VISIBLE);
                viewAll.setVisibility(View.VISIBLE);
            }
            else
            {
                Toast.makeText(getContext(),"You have forgotten to fill out one of these fields",Toast.LENGTH_LONG).show();
            }
        }
        });
        return view;
    }
}
