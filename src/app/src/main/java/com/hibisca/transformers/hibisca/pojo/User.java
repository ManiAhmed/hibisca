package com.hibisca.transformers.hibisca.pojo;

/**
 * This is a class that will be used
 * to create and store a users credentials
 * in the database base by using the class
 * members are individual fields in the
 * database.
 */

public class User
{

    private String firstName;
    private String lastName;
    private String email;
    private String username;
    private String password;
    private String postalCode;

    public User(){

    }
    /**
     * Arg Constructor for creating a new user and their profile information.
     * @param firstName The users first name.
     * @param lastName  The users last name.
     * @param email     The users email address. Which may be used as a primary key.
     * @param username  The *unique* username. Which may be used as part of a composite primary key.
     * @param password  The users(encrypted?)passowrd. *SASHA - note to self - create a basic encryption algorithm
     *                  between password storage and retrieval to and from the database to create some security within
     *                  the application.
     */
    public User(String firstName, String lastName, String email, String username, String password, String postalCode)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.username = username;
        this.password = password;
        this.postalCode = postalCode;
    }

    /**
     *  Retrieves the users first name.
     * @return Return the users first name.
     */
    public String getFirstName()
    {
        return firstName;
    }

    /**
     *  Sets the users firstname. ***Also this and all (setters/getters) will also have to update the
     *  database information in the future.***
     * @param firstName
     */
    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }

    /**
     * Retrieves the users last name
     * @return Returns the users last name.
     */
    public String getLastName()
    {
        return lastName;
    }

    /**
     * Sets the users last name
     * @param lastName the new last name of the user.
     */
    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }

    /**
     * Retrieves the users email address.
     * @return Returns the users email address.
     */
    public String getEmail()
    {
        return email;
    }

    /**
     * Sets the users email address.
     * @param email the new email address associated with a specific user.
     */
    public void setEmail(String email)
    {
        this.email = email;
    }

    /**
     * Retrieves the users 'Username'
     * @return Retunrns the users 'Username'
     */
    public String getUsername()
    {
        return username;
    }

    /**
     * Sets the users 'Username'
     * @param username the new username associated with this profile.
     */
    public void setUsername(String username)
    {
        this.username = username;
    }

    /**
     * Retrieves the users password.
     * @return Returns the users password.
     */
    public String getPassword()
    {
        return password;
    }

    /**
     * Sets the users password.
     * @param password the new password for this user.
     */
    public void setPassword(String password)
    {
        this.password = password;
    }

    /**
     * Displays the fields withing the *user object.
     * @return The string to be displayed.
     */
    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", postalCode='" + postalCode + '\'' +
                '}';
    }
}
