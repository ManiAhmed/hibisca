package com.hibisca.transformers.hibisca.pojo;

/**
 * Class for storing information on temperature readings.
 */

public class Temperature {
    private String plantID;
    private double temperature;

    /**
     * Gets the plant ID
     * @return the plant ID
     */
    public String getPlantID() {
        return plantID;
    }

    /**
     * Sets the plant ID
     * @param plantID the plant ID
     */
    public void setPlantID(String plantID) {
        this.plantID = plantID;
    }

    /**
     * Gets the temperature reading
     * @return the temperature reading
     */
    public double getTemperature() {
        return temperature;
    }

    /**
     * Sets the temperature reading
     * @param temperature the temperature reading
     */
    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    public Temperature(String plantID, double temperature) {
        this.plantID = plantID;
        this.temperature = temperature;
    }

    public Temperature() {

    }
}
