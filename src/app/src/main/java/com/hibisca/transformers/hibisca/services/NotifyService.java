package com.hibisca.transformers.hibisca.services;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import com.hibisca.transformers.hibisca.R;

/**
 *  This class will create a service for our
 *  alert systems notifications.
 */
public class NotifyService extends Service
{
    @Override
    public IBinder onBind(Intent intent)
    {
        throw new UnsupportedOperationException("Not yet implemented");
    }


    @Override
    public void onCreate()
    {

    }

    /**
     * This method will be called to handle and create a notification
     * on the correct date associated with our calendar.
     * @param intent the intent that called/requested the service.
     * @param flags unused in our current conecxt but may be used to indicate, satrisfy a certain condidition
     * @param startId a unique tag for the service..?
     * @return
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        NotificationChannel channel_one = new NotificationChannel("ch_1", "Button Notification", NotificationManager.IMPORTANCE_HIGH);
        NotificationManager notify = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notify.createNotificationChannel(channel_one);
        NotificationCompat.Builder notBuild = new NotificationCompat.Builder(getApplicationContext(), "ch_1");
        notBuild.setSmallIcon(R.drawable.ic_launcher_background);
        notBuild.setContentText(intent.getStringExtra("strDesc"));
        notBuild.setContentTitle(intent.getStringExtra("alertID"));
        notify.notify(666, notBuild.build());
        return START_STICKY;
    }

}
