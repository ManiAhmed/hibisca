package com.hibisca.transformers.hibisca.screens;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hibisca.transformers.hibisca.MenuActivity;
import com.hibisca.transformers.hibisca.R;
import com.hibisca.transformers.hibisca.adapters.PlaylistAdapter;
import com.hibisca.transformers.hibisca.checkconnect.IConnect;
import com.hibisca.transformers.hibisca.interfaces.OnPlaylistClicked;
import com.hibisca.transformers.hibisca.spotifypojo.PlayListItem;
import com.spotify.sdk.android.authentication.AuthenticationClient;
import com.spotify.sdk.android.authentication.AuthenticationRequest;
import com.spotify.sdk.android.authentication.AuthenticationResponse;
import com.spotify.sdk.android.player.Config;
import com.spotify.sdk.android.player.ConnectionStateCallback;
import com.spotify.sdk.android.player.Error;
import com.spotify.sdk.android.player.Player;
import com.spotify.sdk.android.player.Spotify;
import com.spotify.sdk.android.player.SpotifyPlayer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * This screen will allow the user to log into their Spotify account
 * and play music from their list of current playlists (must have a
 * premium Spotify account)
 */

public class PlaylistScreen extends MenuActivity implements ConnectionStateCallback, OnPlaylistClicked{

    private static final String url = "https://api.spotify.com/v1/me/playlists";
    private static final String CLIENT_ID = "6437b8aa9f7b45db81f6dbce224a4d9a";
    private static final String REDIRECT_URI = "hibisca://callback";
    private static final int REQUEST_CODE = 100;
    private Player mPlayer;
    private String accessToken;
    private JSONObject jsonObject;
    ArrayList<PlayListItem> spotifyItems;
    RecyclerView playlistRecyclerView;
    PlaylistAdapter playlistAdapter;
    ImageView playNext;
    ImageView playPrev;
    ImageView pause;
    ImageView play;

    /**
     * Creates the login screen from Spotify for user to log in with
     * It also sets the authorization scopes of the login request which
     * are the features the application will have access to
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_playlist_screen);
        setTitle(R.string.titleMusic);
        //spotifyItems = null;
        playNext = findViewById(R.id.imgNext);
        playPrev = findViewById(R.id.imgPrev);
        pause = findViewById(R.id.imgPause);
        play = findViewById(R.id.imgPlay);


        IConnect checkConnection  = new IConnect(getBaseContext());
        boolean isConnected = checkConnection.checkForConnection();
        Log.i("9193151414532054",String.valueOf(isConnected));
        if(isConnected == true) {
            AuthenticationRequest.Builder loginRequest = new AuthenticationRequest.Builder(CLIENT_ID, AuthenticationResponse.Type.TOKEN, REDIRECT_URI);
            loginRequest.setShowDialog(true);
            loginRequest.setScopes(new String[]{"playlist-read-private", "user-read-private", "streaming"});
            AuthenticationRequest request = loginRequest.build();

            AuthenticationClient.openLoginActivity(this, REQUEST_CODE, request);
        }
    }

    /**
     * Gets the Authorization Code from spotify when the user logs in. Also
     * initializes the player and the click listener for playing the next song
     * @param requestCode
     * @param resultCode
     * @param intent
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        AuthenticationResponse response = null;
        if (requestCode == REQUEST_CODE) {
            response = AuthenticationClient.getResponse(resultCode, intent);
            accessToken = response.getAccessToken();
            if (response.getType() == AuthenticationResponse.Type.TOKEN) {
                Config playerConfig = new Config(this, response.getAccessToken(), CLIENT_ID);
                Spotify.getPlayer(playerConfig, this, new SpotifyPlayer.InitializationObserver() {
                    @Override
                    public void onInitialized(SpotifyPlayer spotifyPlayer) {
                        mPlayer = spotifyPlayer;
                        mPlayer.addConnectionStateCallback(PlaylistScreen.this);
                        playNext.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mPlayer.skipToNext(null);
                            }
                        });
                        playPrev.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mPlayer.skipToPrevious(null);
                            }
                        });
                        pause.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mPlayer.pause(null);
                            }
                        });
                        play.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mPlayer.resume(null);
                            }
                        });
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        Log.e("PlaylistScreen", "Could not initialize player: " + throwable.getMessage());
                    }
                });

            }
        }
    }

    /**
     * Plays the playlist when user clicks on a playlist. It also shuffles
     * the playlist (spotify only allows shuffle after the first song)
     * @param position
     */
    @Override
    public void onPlaylistClicked(int position) {
        PlayListItem playlist = spotifyItems.get(position);

        String playlistId = playlist.getId();
        mPlayer.setShuffle(null, true);
        mPlayer.playUri(null, "spotify:playlist:" + playlistId, 0, 0);
        Log.d("LIST", spotifyItems.toString());

    }

    /**
     * Private class that performs an Async Task that makes a network call
     * to spotify to gather the information needed
     */
    private class SpotifyAPICall extends AsyncTask<String, Void, JSONObject> {

        /**
         * API call that gathers the user's current playlists. The API call is then
         * parsed to JSON objects.
         */
        @Override
        protected JSONObject doInBackground(String... strings) {
            StringBuilder builder = new StringBuilder();
            if(spotifyItems == null) {
                try {

                    URL spotifyURL = new URL(url);
                    HttpURLConnection urlConnection = (HttpURLConnection) spotifyURL.openConnection();
                    Log.d("PlaylistScreen", accessToken);
                    urlConnection.setRequestProperty("Authorization", "Bearer " + accessToken);
                    urlConnection.connect();
                    InputStream inputStream = urlConnection.getInputStream();

                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        Log.d("Playlist", line);
                        builder.append(line).append("\n");
                    }

                } catch (MalformedURLException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }


                try {
                    jsonObject = new JSONObject(builder.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return jsonObject;
            }

            return null;
        }

        /**
         *
         * @param jsonObject
         */
        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            if (jsonObject != null)
            {
                try {
                    JSONArray spotifylist = jsonObject.getJSONArray("items");
                    spotifyItems = PlayListItem.fromJson(spotifylist);
                    Log.d("Execute", spotifyItems.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }

                showListView();
            }
        }
    }

    /**
     * Display the list of playlists that were either saved in the Shared Preferences
     * or were called by the network into a RecyclerView
     */
    public void showListView(){
        Log.d("Playlist", spotifyItems.size() + "");
        playlistRecyclerView = findViewById(R.id.playlistRecyclerView);
        playlistAdapter = new PlaylistAdapter(spotifyItems);
        playlistRecyclerView.setAdapter(playlistAdapter);
        playlistRecyclerView.setLayoutManager(new GridLayoutManager(this, 3));
    }

    /**
     * The following methods are implemented by the ConnectionStateCallback
     * interface. These are different connection state events that Spotify
     * has implemented
     */
    @Override
    public void onLoggedIn() {
        SpotifyAPICall spotifyAPICall = new SpotifyAPICall();
        spotifyAPICall.execute(url);
    }

    @Override
    public void onLoggedOut() {

    }

    @Override
    public void onLoginFailed(Error error) {

    }

    @Override
    public void onTemporaryError() {

    }

    @Override
    public void onConnectionMessage(String s) {

    }

    /**
     * Gets the Shared Preferences if there are any that are saved
     * from a previous session. It then turns the shared preferences back to
     * the array list using GSON to convert the String to an Array List of PlayListItem.
     * Lastly it displays them again in the RecyclerView
     */
    public void reInitializeArrayList(){
        if(spotifyItems == null) {
            SharedPreferences preferences = this.getSharedPreferences("Playlists", Context.MODE_PRIVATE);
            Gson gson = new Gson();
            String json = preferences.getString("playlists", "");
            Type type = new TypeToken<ArrayList<PlayListItem>>() {
            }.getType();
            spotifyItems = gson.fromJson(json, type);
            if(spotifyItems != null) {
                Log.d("Restart", spotifyItems.toString());
                showListView();
            }
        }
    }

    /**
     * On Pause the data will be saved into a file called "Playlists" using Shared Preferences
     * the Array List of playlists is parsed to a string using GSON and saved to the playlist.
     * Then the edit is committed which stores the playlists internally
     */
    @Override
    protected void onPause() {
        super.onPause();
        SharedPreferences preferences = this.getSharedPreferences("Playlists", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        Gson gson = new Gson();
        String jsonPlaylists = gson.toJson(spotifyItems);
        Log.d("TAG", jsonPlaylists);
        editor.putString("playlists", jsonPlaylists);
        editor.commit();
    }

    @Override
    protected void onStop(){

        super.onStop();
    }

    @Override
    protected void onResume() {
        super.onResume();
        reInitializeArrayList();
    }

    @Override
    protected void onDestroy(){
        Spotify.destroyPlayer(this);
        super.onDestroy();
    }

}
