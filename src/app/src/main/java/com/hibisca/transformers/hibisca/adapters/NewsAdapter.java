package com.hibisca.transformers.hibisca.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hibisca.transformers.hibisca.R;
import com.hibisca.transformers.hibisca.customviews.NewsView;
import com.hibisca.transformers.hibisca.pojo.NewsObject;
import com.hibisca.transformers.hibisca.screens.ActivityWeb;

import java.util.ArrayList;

/**
 * Created by cstuser on 5/1/2018.
 */

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewHolder> {
    private ArrayList<NewsObject> newsObjects;
   // private Context context;

    /**
     * Creates a news adapter with the list of news objects passed
     * @param newsObjectsList
     */
    public NewsAdapter(ArrayList<NewsObject> newsObjectsList) {
        try {
            this.newsObjects = newsObjectsList;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public NewsView newsView;

        /**
         * Sets a view to a newly created view holder
         * @param view the view to set to the holder
         */
        public ViewHolder(View view) {
            super(view);
            try {
                newsView = (NewsView)view.findViewById(R.id.newsListItem);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Creates a view holder for each news item
     * @param parent parent recyclerview viewgroup
     * @param viewType type of the view
     * @return view holder for a single news item
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder viewHolder = null;
        try {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View newsView = inflater.inflate(R.layout.news_item_layout, parent, false);
            viewHolder = new ViewHolder(newsView);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return viewHolder;
    }

    /**
     * Binds the view to the view holder
     * @param holder view holder for the news view
     * @param position position of the news item in the list
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        try {
            holder.newsView.setTitle(newsObjects.get(position).getTitle());
            holder.newsView.setContentDesc(newsObjects.get(position).getDescription());
            holder.newsView.setSource(newsObjects.get(position).getSource());
            holder.newsView.setNewsImage(newsObjects.get(position).getNewsImage());
            holder.newsView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Context context = view.getContext();
                    //Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(newsObjects.get(position).getUrl()));
                    Intent intent = new Intent(context, ActivityWeb.class);
                    intent.putExtra("url",newsObjects.get(position).getUrl());
                    context.startActivity(intent);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the image to the correct news item in the list
     * @param index index of the news item in the list
     * @param bitmap image of the corresponding news item
     */
    public void setImage(int index, Bitmap bitmap) {
        try {
            newsObjects.get(index).setNewsImage(bitmap);
            notifyItemChanged(index);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the number of news items in the list
     * @return size of array list with news items
     */
    @Override
    public int getItemCount() {
        try {
            return newsObjects.size();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
}
