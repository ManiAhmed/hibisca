
package com.hibisca.transformers.hibisca.database.controllers;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.hibisca.transformers.hibisca.database.DatabaseAccess;
import com.hibisca.transformers.hibisca.pojo.Plant;

import java.util.ArrayList;
import java.util.List;

public class PlantsController
{
    /**
     *  A class for controlling interactions between an Android View (such as an activity/fragment),
     *  a model (that is, contact model) and an SQLite database. *
     * Created by Sleiman on 3/30/2018.
     */

    private DatabaseAccess databaseAccessHelper;
    private SQLiteDatabase sqLiteDatabase;
    private static final String TAG = PlantsController.class.getSimpleName();

    /**
     *  Creates a controller for controlling the interactions between a view and the plants model.
     * @param context The application context that must be passed from an android view.
     */
    public PlantsController(Context context)
    {
        this.databaseAccessHelper = DatabaseAccess.getInstance(context);
    }

    public long insertPlant(Plant plant)
    {
        //-- 1) Open the database.
        sqLiteDatabase = this.databaseAccessHelper.open();
        sqLiteDatabase.beginTransaction();
        long success = 0;
        try
        {
            ContentValues values = new ContentValues();
            values.put("PlantId", plant.getPlantID() );
            values.put("PlantName", plant.getPlantName());
            values.put("Species", plant.getSpecies());
            values.put("Username", plant.getUsername());
            success = sqLiteDatabase.insert("Plants", null, values);
            sqLiteDatabase.setTransactionSuccessful();
        }
        catch (Exception e)
        {
            Log.wtf(TAG, e.fillInStackTrace());
        }
        finally
        {
            //-- 3) Very important, closeDatabase and unlock the database file.
            sqLiteDatabase.endTransaction();
            this.databaseAccessHelper.close();
            return success;
        }
    }

    /**
     * Reads all plants from the database.
     * @return a List of plants.
     */
    public List<Plant> getPlants()
    {
        sqLiteDatabase = this.databaseAccessHelper.open();
        sqLiteDatabase.beginTransaction();
        List<Plant> plants = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM Plants", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast())
        {
            Plant plant = new Plant();
            plant.setPlantID(cursor.getString(0));
            plant.setPlantName(cursor.getString(1));
            plant.setSpecies(cursor.getString(2));
            plant.setUsername(cursor.getString(3));
            plants.add(plant);
            cursor.moveToNext();
        }
        cursor.close();
        sqLiteDatabase.endTransaction();
        this.databaseAccessHelper.close();
        return plants;
    }

    /**
     * Deletes the specified plant from the database.
     * @param plntID the string that will be used to delete its corresponding
     * record from the database.
     */
    public void deletePlant(String plntID)
    {
        //-- 1) Open the database.
        sqLiteDatabase = this.databaseAccessHelper.open();
        try {
            sqLiteDatabase.execSQL("DELETE FROM " + "Plants" + " WHERE" + " PlantId" + "=\"" + plntID + "\";");
        } catch (Exception e) {
            Log.wtf(TAG, e.fillInStackTrace());
        } finally {
            sqLiteDatabase.close();
        }
    }
}
