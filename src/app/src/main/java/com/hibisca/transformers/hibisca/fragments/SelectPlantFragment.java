package com.hibisca.transformers.hibisca.fragments;

import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.hibisca.transformers.hibisca.adapters.MyPlantsListAdapter;
import com.hibisca.transformers.hibisca.database.controllers.PlantsController;
import com.hibisca.transformers.hibisca.pojo.Plant;
import com.hibisca.transformers.hibisca.R;

import java.util.List;

/**
 * Created by cstuser on 4/18/2018.
 */

/**
 * This class will be used to allow the user to select the
 * plant that will be used to stored data  to the database
 * based on various sensor readings
 * so that when they are displayed by the graph screens
 * the output belongs to a specific plant. This fragment will
 * also be used by the graph page so that the user can select the
 * plant they with to analyze / see the data for.
 */

public class SelectPlantFragment extends DialogFragment {

    /**
     * Will pass the plant that was select back to the activity that called the fragment.
     */
    public interface SelectPlantListener{
        void selectPlant(Plant plant);
    }
    private PlantsController plantsController;
    private List<Plant> plantList;
    private ListView listView;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        try {

            return inflater.inflate(R.layout.select_plant_layout, container, false);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        plantsController = new PlantsController(getActivity());
        plantList = plantsController.getPlants();

        MyPlantsListAdapter listAdapter = new MyPlantsListAdapter(getActivity(), plantList);
        listView = (ListView)view.findViewById(R.id.plantListView);
        listView.setAdapter(listAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                int ID = listView.getPositionForView(view);

                Plant plant = plantList.get(listView.getPositionForView(view));
                SelectPlantListener selectListener = (SelectPlantListener) getActivity();
                selectListener.selectPlant(plant);//passes the plant id that was selected
                dismiss();
            }
        });
    }
}
