package com.hibisca.transformers.hibisca.screens;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.hibisca.transformers.hibisca.database.controllers.ProfileController;
import com.hibisca.transformers.hibisca.pojo.User;
import com.hibisca.transformers.hibisca.pojo.UserSession;
import com.hibisca.transformers.hibisca.R;

/**
 * This is screen is where first-time users will be able to sign up for
 * the application.
 */

public class SignUpScreen extends AppCompatActivity {

    private static final String EMAIL_REGEX = "^\\w+@\\w+\\.[a-z]{2,3}$";
    private static final String POSTALCODE_REGEX = "^[A-Z]\\d[A-Z]\\s\\d[A-Z]\\d$";
    private static final String FIRST_NAME_REGEX = "^[a-zA-Z]{1,15}$";
    private static final String LAST_NAME_REGEX = "^[a-zA-Z]{1,25}$";
    private Pattern emailPattern;
    private Matcher emailMatcher;
    private Pattern firstNamePattern;
    private Matcher firstNameMatcher;
    private Pattern lastNamePattern;
    private Matcher lastNameMatcher;
    private Pattern postalCodePattern;
    private Matcher postalCodeMatcher;

    EditText txtFirstName;
    EditText txtLastName;
    EditText txtEmail;
    EditText txtPostalCode;
    EditText txtUsername;
    EditText txtPassword;
    EditText txtConfirmPassword;
    Button cncl;
    Button affirm;
    ProfileController profileController;

    /**
     * Initializes the cancel and start buttons
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_screen);
        setTitle(R.string.titleSignUp);
        cncl =(Button)findViewById(R.id.btnCancel);
        affirm = (Button)findViewById(R.id.btnStart);
        profileController = new ProfileController(this);
    }

    /**
     * Dismisses the sign up screen and returns to the home screen
     * @param view
     */
    public void onClickCancel(View view)
    {
        final MediaPlayer bwd = android.media.MediaPlayer.create(this, R.raw.backward);
        bwd.start();
        Intent goBackHome = new Intent(this, HomeScreen.class);
        startActivity(goBackHome);
        finish();
    }

    /**
     * After a lot of error handling and form validation, the user will be added to the database
     * and can start using the application as it will go to the welcome screen of our application
     * @param view
     */
    public void onClickStart(View view)
    {
        boolean isUnique;
        final MediaPlayer fwd = android.media.MediaPlayer.create(this, R.raw.forward);
        fwd.start();

        txtFirstName = this.findViewById(R.id.txtFirstName);
        txtLastName = this.findViewById(R.id.txtLastName);
        txtEmail = this.findViewById(R.id.txtEmail);
        txtPostalCode = this.findViewById(R.id.txtPostalCode);
        txtUsername = this.findViewById(R.id.txtUsername);
        txtPassword = this.findViewById(R.id.txtPassword);
        txtConfirmPassword = this.findViewById(R.id.txtConfirmPassword);

        emailPattern = Pattern.compile(EMAIL_REGEX);
        emailMatcher = emailPattern.matcher(txtEmail.getText().toString());

        postalCodePattern = Pattern.compile(POSTALCODE_REGEX);
        postalCodeMatcher = postalCodePattern.matcher(txtPostalCode.getText().toString());

        firstNamePattern = Pattern.compile(FIRST_NAME_REGEX);
        firstNameMatcher = firstNamePattern.matcher(txtFirstName.getText().toString());

        lastNamePattern = Pattern.compile(LAST_NAME_REGEX);
        lastNameMatcher = lastNamePattern.matcher(txtLastName.getText().toString());

        if(! firstNameMatcher.matches())
            Toast.makeText(this, R.string.errFirst, Toast.LENGTH_LONG).show();

        else if(! lastNameMatcher.matches())
            Toast.makeText(this, R.string.errLast, Toast.LENGTH_LONG).show();

        else if(txtUsername.getText().toString().length() < 8 || txtUsername.getText().toString().length() > 15)
            Toast.makeText(this, R.string.errUsername, Toast.LENGTH_LONG).show();

        else if(! emailMatcher.matches())
            Toast.makeText(this, R.string.errEmail, Toast.LENGTH_LONG).show();

        else if(! postalCodeMatcher.matches())
            Toast.makeText(this, R.string.errPostalCode, Toast.LENGTH_LONG).show();

        else if(! txtPassword.getText().toString().equals(txtConfirmPassword.getText().toString()))
            Toast.makeText(this, R.string.errSamePassword, Toast.LENGTH_LONG).show();

        else if(txtPassword.getText().toString().length() < 6 || txtPassword.getText().toString().length() > 12)
            Toast.makeText(this, R.string.errPasswordLength, Toast.LENGTH_LONG).show();

        else
        {
            User newUser = new User();

            String firstName = txtFirstName.getText().toString();
            String lastName = txtLastName.getText().toString();
            String email = txtEmail.getText().toString();
            String username = txtUsername.getText().toString();
            String password = txtPassword.getText().toString();
            String postalCode = txtPostalCode.getText().toString();

            newUser.setUsername(username);
            newUser.setFirstName(firstName);
            newUser.setLastName(lastName);
            newUser.setEmail(email);
            newUser.setPassword(password);
            newUser.setPostalCode(postalCode);

            //Checks to make sure there is no duplicate username in database
            isUnique = profileController.isUserUnique(newUser);
            if(! isUnique)
                Toast.makeText(this, UserSession.getInstance().getSignUpErrMsg(), Toast.LENGTH_LONG).show();

            else {

                // Save the user object into a database
                profileController.insertProfile(newUser);

                // Save instance of the current User
                UserSession.getInstance().setUsername(newUser.getUsername());
                UserSession.getInstance().setFirstName(newUser.getFirstName());
                UserSession.getInstance().setLastName(newUser.getLastName());
                UserSession.getInstance().setEmail(newUser.getEmail());
                UserSession.getInstance().setPostalCode(newUser.getPostalCode());

                Intent welcome = new Intent(this, WelcomeScreen.class);
                startActivity(welcome);
                finish();
            }
        }
    }
}