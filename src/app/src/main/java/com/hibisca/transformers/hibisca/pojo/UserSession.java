package com.hibisca.transformers.hibisca.pojo;

/**
 * Created by harry on 2018-04-14.
 */

public class UserSession extends User {

    private static UserSession instance;

    private String loginErrMsg;
    private String signUpErrMsg;
    private boolean isLoggedIn;

    private  UserSession(){

    }

    public static synchronized  UserSession getInstance() {
        if (instance == null) {
            instance = new UserSession();
        }
        return instance;
    }

    public String getLoginErrMsg() {
        return loginErrMsg;
    }

    public void setLoginErrMsg(String loginErrMsg) {
        this.loginErrMsg = loginErrMsg;
    }

    /**
     * Returns the sign up error message
     * @return signUpErrMsg
     */
    public String getSignUpErrMsg() {
        return signUpErrMsg;
    }

    /**
     * Sets the sign up error message for the user
     * @param signUpErrMsg = the sign up error message
     */
    public void setSignUpErrMsg(String signUpErrMsg) {
        this.signUpErrMsg = signUpErrMsg;
    }

    public boolean isLoggedIn() {
        return isLoggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        isLoggedIn = loggedIn;
    }
}
