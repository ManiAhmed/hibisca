package com.hibisca.transformers.hibisca.database.controllers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.hibisca.transformers.hibisca.database.DatabaseAccess;

import java.util.ArrayList;
import java.util.List;

public class AlertsController
{

    /**
     *  A class for controlling interactions between an Android View (such as an activity/fragment),
     *  a model (that is, contact model) and an SQLite database. *
     * Created by Sleiman on 3/30/2018.
     */

        private DatabaseAccess databaseAccessHelper;
        private SQLiteDatabase sqLiteDatabase;
        private static final String TAG = AlertsController.class.getSimpleName();

        /**
         *  Creates a controller for controlling the interactions between a view and the alerts model.
         * @param context The application context that must be passed from an android view.
         */
        public AlertsController(Context context)
        {
            this.databaseAccessHelper = DatabaseAccess.getInstance(context);
        }

    /**
     * This will insert a new alert into the database
     * @param day the day of the alert
     * @param month the month of the alert
     * @param year the year of the alert
     * @param alrtDesc description of the event
     * @param alrtID the alert id
     * @return 0 if inserted and -1 if alertId already exists
     */
        public long insertAlert(int day, int month, int year,String alrtDesc, String alrtID )
        {
            //-- 1) Open the database.
            sqLiteDatabase = this.databaseAccessHelper.open();
            sqLiteDatabase.beginTransaction();
            long success = 0;
            try
            {
                ContentValues values = new ContentValues();
                values.put("Day", day);
                values.put("Month", month);
                values.put("Year", year);
                values.put("EventDescription",alrtDesc);
                values.put("AlertId", alrtID);
                success = sqLiteDatabase.insert("Alerts", null, values);
                sqLiteDatabase.setTransactionSuccessful();
            }
            catch (Exception e)
            {
                Log.wtf(TAG, e.fillInStackTrace());
            }
            finally
            {
                //-- 3) Very important, closeDatabase and unlock the database file.
                sqLiteDatabase.endTransaction();
                this.databaseAccessHelper.close();
                return success;
            }
        }

        /**
         * Reads all alerts from the database.
         * @return a List of alerts
         */
        public List<String> getAlerts()
        {
            sqLiteDatabase = this.databaseAccessHelper.open();
            sqLiteDatabase.beginTransaction();
            List<String> list = new ArrayList<>();
            String strBuilder = "";
            Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM Alerts ORDER BY Year,Month,Day", null);
            cursor.moveToFirst();
            while (!cursor.isAfterLast())
            {
                strBuilder = "";
                strBuilder += " " + (cursor.getString(cursor.getColumnIndex("Day")));
                strBuilder += " " + (cursor.getString(cursor.getColumnIndex("Month")));
                strBuilder += " " + (cursor.getString(cursor.getColumnIndex("Year")));
                strBuilder += " " + (cursor.getString(cursor.getColumnIndex("AlertId")));
                strBuilder += " " + (cursor.getString(cursor.getColumnIndex("EventDescription")));
                list.add(strBuilder);
                cursor.moveToNext();
            }
            cursor.close();
            sqLiteDatabase.endTransaction();
            this.databaseAccessHelper.close();
            return list;
        }

    /**
     * Reads alert Description for a given event.
     * @return a List of alerts
     */
    public String getAlert(String str)
    {
        sqLiteDatabase = this.databaseAccessHelper.open();
        sqLiteDatabase.beginTransaction();
        String strBuilder = "";
        String query = "SELECT * FROM " + "Alerts" + " WHERE" + " AlertId" + "=\"" + str + "\";";
        Cursor cursor = sqLiteDatabase.rawQuery(query, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast())
        {
            strBuilder = (cursor.getString(cursor.getColumnIndex("EventDescription")));
            cursor.moveToNext();
        }
        cursor.close();
        sqLiteDatabase.endTransaction();
        this.databaseAccessHelper.close();
        return strBuilder;
    }

    /**
     * Read only the alerts from the database
     * that match the given day a user has selected from the calendar
     * view in the AlertScreen.
     * @param today the day that was selected.
     * @param tMonth the month associated with the day that was selected.
     * @param tYear the year associated with the day that was selected
     * @return the list of events to be returned to the caller.
     */
    public List<String> getTodaysAlerts(int today, int tMonth, int tYear)
    {
        sqLiteDatabase = this.databaseAccessHelper.open();
        sqLiteDatabase.beginTransaction();
        List<String> list = new ArrayList<>();
        String strBuilder = "";
        Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM Alerts" + " WHERE Day =" + "\'" + today + "\'"
                                                                            + " AND Month =" +"\'" + tMonth + "\'"
                                                                            +" AND Year =" + "\'" + tYear +"\'" + ";", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast())
        {
            strBuilder = "";
            strBuilder += " " + (cursor.getString(cursor.getColumnIndex("Day")));
            strBuilder += " " + (cursor.getString(cursor.getColumnIndex("Month")));
            strBuilder += " " + (cursor.getString(cursor.getColumnIndex("Year")));
            strBuilder += " " + (cursor.getString(cursor.getColumnIndex("AlertId")));
            strBuilder += " " + (cursor.getString(cursor.getColumnIndex("EventDescription")));
            list.add(strBuilder);
            cursor.moveToNext();
        }
        cursor.close();
        sqLiteDatabase.endTransaction();
        this.databaseAccessHelper.close();
        return list;
    }

        /**
         * Deletes the specified alert from the database.
         * @param alrtID the string that will be used to delete its corresponding
         * record from the database.
         */
        public void deleteAlert(String alrtID)
        {
            //-- 1) Open the database.
            sqLiteDatabase = this.databaseAccessHelper.open();
            try {
                sqLiteDatabase.execSQL("DELETE FROM " + "Alerts" + " WHERE" + " AlertId" + "=\"" + alrtID + "\";");
            } catch (Exception e) {
                Log.wtf(TAG, e.fillInStackTrace());
            } finally {
                sqLiteDatabase.close();
            }
        }
}
