package com.hibisca.transformers.hibisca.pojo;


import android.graphics.Bitmap;

/**
 * Created by cstuser on 5/1/2018.
 */

public class NewsObject {
    private String title;
    private String author;
    private String description;
    private String url;
    private String imageUrl;
    private String source;
    private Bitmap newsImage;

    public NewsObject() {

    }


    public String getTitle() {
        try {
            return title;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setTitle(String title) {
        try {
            this.title = title;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        try {
            this.author = author;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getDescription() {
        try {
            return description;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setDescription(String description) {
        try {
            this.description = description;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getUrl() {
        try {
            return url;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setUrl(String url) {
        try {
            this.url = url;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getImageUrl() {
        try {
            return imageUrl;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setImageUrl(String imageUrl) {
        try {
            this.imageUrl = imageUrl;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getSource() {
        try {
            return "Source: "+ source;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setSource(String source) {
        try {
            this.source = source;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Bitmap getNewsImage() {
        try {
            return newsImage;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void setNewsImage(Bitmap newsImage) {
        try {
            this.newsImage = newsImage;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
