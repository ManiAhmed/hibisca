package com.hibisca.transformers.hibisca.screens;

import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.hibisca.transformers.hibisca.adapters.MyPlantsGridAdapter;
import com.hibisca.transformers.hibisca.database.controllers.PlantsController;
import com.hibisca.transformers.hibisca.fragments.AddPlantFragment;
import com.hibisca.transformers.hibisca.interfaces.addPlantFragmentListener;
import com.hibisca.transformers.hibisca.MenuActivity;
import com.hibisca.transformers.hibisca.pojo.Plant;
import com.hibisca.transformers.hibisca.R;

import java.util.List;

/**
 * Screen where a list (GridView) of users plants are displayed
 */
public class MyPlantsScreen extends MenuActivity implements addPlantFragmentListener {
    GridView grid;
    /**
     * Initializes variables and layout, and gets the right GridView of plants using a custom adapter MyPlantsGridAdapter
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_plants_screen);
        setTitle(R.string.titleMyPlants);

        grid = (GridView) findViewById(R.id.my_plants_grid);
        PlantsController controller = new PlantsController(this);
        List<Plant> plants = controller.getPlants();


        MyPlantsGridAdapter gridAdapter = new MyPlantsGridAdapter(MyPlantsScreen.this, plants);
        grid.setAdapter(gridAdapter);
        registerForContextMenu(grid);
    }

    @Override
    protected void onPause()
    {
        super.onPause();
    }

    /**
     * Shows the AddPlantFragment dialogFragment to prompt the user to add a plant
     * @param view
     */
    public void onAddPlant(View view) {
        AddPlantFragment fragment = new AddPlantFragment();
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragment.show(fragmentManager, "addPlant");
    }

    /**
     * Adds the plant to the database
     * @param plant the plant to be added
     */
    @Override
    public void addPlant(Plant plant) {
        PlantsController controller = new PlantsController(this);
        controller.insertPlant(plant);
        finish();
        startActivity(getIntent());
    }

    /**
     * Creates the context menu for each list item
     * @param menu
     * @param v
     * @param menuInfo
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.list_plants_menu, menu);
    }

    /**
     * Deletes the selected plant when the menu option delete is selected
     * @param item the item selected
     * @return true if successfully removed from the database
     */
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        //When the user clicks delete on a certain list item it will remove it from the database and refresh the activity.
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        Plant plant = (Plant) grid.getItemAtPosition(info.position);
        PlantsController controller = new PlantsController(this);
        controller.deletePlant(plant.getPlantID());

        finish();
        startActivity(getIntent());
        return true;
    }
}
