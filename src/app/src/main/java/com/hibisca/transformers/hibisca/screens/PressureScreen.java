package com.hibisca.transformers.hibisca.screens;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.hibisca.transformers.hibisca.MenuActivity;
import com.hibisca.transformers.hibisca.R;
import com.hibisca.transformers.hibisca.customviews.NeedleView;
import com.hibisca.transformers.hibisca.database.controllers.PressureController;
import com.hibisca.transformers.hibisca.fragments.SelectPlantFragment;
import com.hibisca.transformers.hibisca.pojo.Plant;
import com.hibisca.transformers.hibisca.pojo.Pressure;

/**
 * Created by cstuser on 5/7/2018.
 */

public class PressureScreen extends MenuActivity implements SensorEventListener, SelectPlantFragment.SelectPlantListener{


    private SensorManager pressureSensorManager;
    private PressureController controller;
    private Sensor pressureSensor;
    private TextView displayPressure;
    private NeedleView needle;
    private Button getPressure;
    private Button readForPlant;
    private int pressureReading;
    private Plant selectedPlant = null;
    private Button deleteReading;


    /**
     * Initializes the various variables that will be needed for the screen
     * as well setting the listeners for the button to turning the sensor on
     * and off
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pressure_screen);
        setTitle("");
        pressureSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        controller = new PressureController(this);
        pressureSensor = pressureSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
        displayPressure = findViewById(R.id.displayPressure);
        getPressure= (Button) findViewById(R.id.btnPressure);
        readForPlant = (Button) findViewById(R.id.btnReadForPlant);
        needle = (NeedleView) findViewById(R.id.needleView);
        deleteReading = (Button)findViewById(R.id.deleteReading);

        getPressure.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPressure.setText(R.string.btnStopReading);
                pressureSensorManager.registerListener(PressureScreen.this, pressureSensor, SensorManager.SENSOR_DELAY_NORMAL);
            }
        });

        getPressure.setOnLongClickListener(
                new View.OnLongClickListener() {
                    @Override
                    public boolean onLongClick(View view) {
                        getPressure.setText(R.string.btnBeginReading);
                        pressureSensorManager.unregisterListener(PressureScreen.this, pressureSensor);
                        if (selectedPlant != null)
                            storeValue(pressureReading);
                        return true;
                    }
                });

        readForPlant.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager manager = getSupportFragmentManager();
                SelectPlantFragment selectPlant = new SelectPlantFragment();
                selectPlant.setCancelable(true);
                selectPlant.show(manager, "pressure");
            }
        });

        deleteReading.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deletePressureReading();
            }
        });


    }

    /**
     * Retreives the selected plant
     * @param plant the plant the user selected
     */
    public void selectPlant(Plant plant) {
        Log.v("SELECTED", "SEL: " + plant.getPlantName());
        selectedPlant = new Plant(plant);
    }

    /**
     * Stores the current reading for pressure in the database
     * @param pressureReading the last known pressure reading
     * @return true if stored
     */
    public boolean storeValue(int pressureReading) {

        Pressure pressure = new Pressure(selectedPlant.getPlantID(), pressureReading);
        PressureController controller = new PressureController(this);
        controller.insertPressureReading(pressure);
        selectedPlant = null;
        return true;
    }

    /**
     * Removes a pressure reading from the database
     */
    public void deletePressureReading() {
        if(selectedPlant == null)
            Toast.makeText(this, R.string.selectPlantToDelete, Toast.LENGTH_SHORT).show();
        else {
            controller.deletePressureReading(selectedPlant.getPlantID());
        }
    }

    /**
     * Is called when the accuracy of the sensor is changed
     * @param sensor
     * @param accuracy
     */
    @Override
    public final void onAccuracyChanged(Sensor sensor, int accuracy)
    {

    }

    /**
     * Is called when the pressure value changes
     * Changes the text in the gauge as well as changes the
     * needle position
     * @param event
     */
    @Override
    public final void onSensorChanged(SensorEvent event)
    {
        float degrees = 0;
        displayPressure.setText(Float.toString(event.values[0]) + "%");
        pressureReading = Math.round(event.values[0]);

        degrees = ((float)pressureReading/1100);
        degrees = degrees * 265;
        degrees = degrees - 225;

        Animation an;
        an = new RotateAnimation(0.0f, degrees, needle.getWidth()/2, needle.getHeight()/2);

        an.setDuration(10000);
        an.setRepeatCount(0);
        an.setRepeatMode(Animation.REVERSE);
        an.setFillAfter(true);

        needle.setAnimation(an);
        needle.startAnimation(an);

    }

    /**
     * When Paused, the Sensor Manager stops looking for the pressure in the area
     */
    @Override
    protected void onPause()
    {
        super.onPause();
        pressureSensorManager.unregisterListener(this);
    }

    @Override
    protected void onStop(){
        Log.d("Stop", "Stop");
        super.onStop();
    }
    @Override
    protected void onDestroy() {
        Log.d("Destroy", "Destroy");
        super.onDestroy();
    }
}
