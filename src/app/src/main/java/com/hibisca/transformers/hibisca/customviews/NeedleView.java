package com.hibisca.transformers.hibisca.customviews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by cstuser on 5/7/2018.
 */

public class NeedleView extends View {
    private Paint linePaint;
    private Path linePath;

    private Matrix matrix;
    private float rotation;

    public NeedleView(Context context) {
        super(context);

        matrix = new Matrix();
        init();
    }

    /**
     * Initializes the visual for the view
     */
    private void init() {
        linePaint = new Paint();

        linePaint.setColor(Color.RED);
        linePaint.setStyle(Paint.Style.FILL_AND_STROKE);
        linePaint.setStrokeWidth(5.0f);

        linePath = new Path();
        linePath.moveTo(50.0f, 50.0f);
        linePath.lineTo(130.0f, 40.0f);
        linePath.lineTo(600.0f, 50.0f);
        linePath.lineTo(130.0f, 60.0f);
        linePath.lineTo(50.0f, 50.0f);
        linePath.close();

        linePath.moveTo(50.0f, 50.0f);
    }

    public NeedleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        matrix = new Matrix();
        init();
    }

    public NeedleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        matrix = new Matrix();
        init();
    }

    public NeedleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        matrix = new Matrix();
        init();
    }

    /**
     * Sets the rotation angle of the view
     * @param rotation the angle to rotate the view
     */
    @Override
    public void setRotation(float rotation) {
        super.setRotation(rotation);
    }

    /**
     * Draws the view onto the android canvas
     * @param canvas the canvas object
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.concat(matrix);
        canvas.drawPath(linePath, linePaint);
    }


}
