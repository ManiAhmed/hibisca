package com.hibisca.transformers.hibisca.adapters;

import android.content.Context;
import android.net.wifi.p2p.WifiP2pManager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hibisca.transformers.hibisca.R;
import com.hibisca.transformers.hibisca.interfaces.OnPlaylistClicked;
import com.hibisca.transformers.hibisca.spotifypojo.PlayListItem;

import java.util.ArrayList;

public class PlaylistAdapter extends RecyclerView.Adapter<PlaylistAdapter.ViewHolder> {

    private ArrayList<PlayListItem> listOfPlaylists;
    private Context context;
    private int position = -1;
    private OnPlaylistClicked onPlaylistClicked;
    private CardView playlistCardView;

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView playlistName;
        CardView playlistCardView;

        /**
         * Sets the view to a newly create view holder
         *
         * @param itemView the view to be set in the view holder
         */
        public ViewHolder(View itemView) {
            super(itemView);

            playlistName = itemView.findViewById(R.id.playlistName);
            playlistCardView = itemView.findViewById(R.id.playlistCardView);
        }

    }


    /**
     * Constructor to set the playlist list
     * @param listOfPlaylists playlist list
     */
    public PlaylistAdapter(ArrayList<PlayListItem> listOfPlaylists) {
        this.listOfPlaylists = listOfPlaylists;
    }

    /**
     * Creates a view holder for each news item
     * @param parent parent recyclerview viewgroup
     * @param viewType type of the view
     * @return view holder for a single news item
     */
    @Override
    public PlaylistAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View itemView = inflater.inflate(R.layout.playlist_item_row, parent, false);

        ViewHolder viewHolder = new ViewHolder(itemView);
        return viewHolder;
    }

    /**
     * Binds the view to the view holder
     * @param holder view holder for the news view
     * @param index position of the item in the playlist list
     */
    @Override
    public void onBindViewHolder(final PlaylistAdapter.ViewHolder holder, final int index) {
        PlayListItem playlist = listOfPlaylists.get(index);

        playlistCardView = holder.playlistCardView;
        TextView name = holder.playlistName;
        name.setText(playlist.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            /**
             * Called when a view is clicked
             *
             * @param view the view clicked
             */
            @Override
            public void onClick(View view) {
                playlistCardView.setBackgroundColor(context.getColor(R.color.goldenYellow));
                position = index;
                notifyDataSetChanged();
                Log.d("Adapter", position + "");
                onPlaylistClicked = (OnPlaylistClicked) context;
                onPlaylistClicked.onPlaylistClicked(position);
            }
        });

        if(position == index)
            playlistCardView.setBackgroundColor(context.getColor(R.color.goldenYellow));

        else
            playlistCardView.setBackgroundColor(context.getColor(R.color.backgroundWhite));
    }

    /**
     * Returns the number of playlist items in the list
     * @return size of array list with playlist items
     */
    @Override
    public int getItemCount() {
        return listOfPlaylists.size();
    }
}
