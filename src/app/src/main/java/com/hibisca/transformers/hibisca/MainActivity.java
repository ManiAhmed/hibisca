package com.hibisca.transformers.hibisca;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ProgressBar;

import com.hibisca.transformers.hibisca.screens.HomeScreen;

/**
 * This Activity Opens up the splash screen layout for out application.
 * It also plays a sound file and displays a progress bar. The progress
 * bar uses a thread object to match the progress made in the progress bar
 * to the duration of the sound being played in the background.
 */
public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);

        final MediaPlayer splsh = MediaPlayer.create(this, R.raw.splash);

        final ProgressBar pb = (ProgressBar)findViewById(R.id.progressBar);
        pb.setMax(100);
        pb.setProgress(0);

        splsh.start();

        Thread pgBar = new Thread(new Runnable()
        {
            @Override
            public void run()
            {
                int progress = 0;
                while(progress < 100)
                {
                    try
                    {
                        progress++;
                        pb.setProgress(progress);
                        Thread.sleep(60);
                    }
                    catch(Exception ex)
                    {
                    }
                }
                MainActivity.this.runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        Intent intent = new Intent(MainActivity.this, HomeScreen.class);
                        startActivity(intent);
                        finish();
                    }
                });
            }
        });
        pgBar.start();
    }
}
