package com.hibisca.transformers.hibisca.screens;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.hibisca.transformers.hibisca.MenuActivity;
import com.hibisca.transformers.hibisca.R;

public class ActivityWeb extends MenuActivity {
    private WebView webView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        webView = (WebView)findViewById(R.id.webview);
        webView.loadUrl(getIntent().getStringExtra("url"));
    }
}
