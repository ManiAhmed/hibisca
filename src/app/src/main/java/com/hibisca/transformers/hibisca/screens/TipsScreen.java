package com.hibisca.transformers.hibisca.screens;

import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.hibisca.transformers.hibisca.MenuActivity;
import com.hibisca.transformers.hibisca.R;

/**
 *  This screen enables the user to look through a couple of the
 *  basics of learning how to take care of plants. The screens has buttons
 *  that will pop up alert boxes with little tips on what they need to know
 *  in order to start growing plants.
 */
public class TipsScreen extends MenuActivity {

    Button btnWhatYouNeed;
    Button btnWhatToGrow;
    Button btnPottingPlants;
    Button btnGardenCare;

    /**
     * Initializaes all four buttons as well as creates the listener for each
     * button. When pressed, these buttons have messages that will
     * display on the screen informing the user about growing plants.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tips_screen);
        setTitle(R.string.titleTips);

        btnWhatYouNeed = findViewById(R.id.btnWhatYouNeed);
        btnWhatToGrow = findViewById(R.id.btnWhatToGrow);
        btnPottingPlants = findViewById(R.id.btnPottingPlants);
        btnGardenCare = findViewById(R.id.btnGardenCare);

        btnWhatYouNeed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder msgWhatYouNeed = new AlertDialog.Builder(TipsScreen.this);
                msgWhatYouNeed.setTitle(R.string.btnWhatYouNeed);
                msgWhatYouNeed.setMessage(R.string.txtWhatYouNeed);
                msgWhatYouNeed.setPositiveButton(R.string.btnThanks, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

                msgWhatYouNeed.show();
            }
        });

        btnWhatToGrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder msgWhatToGrow = new AlertDialog.Builder(TipsScreen.this);
                msgWhatToGrow.setTitle(R.string.btnWhatToGrow);
                msgWhatToGrow.setMessage(R.string.txtWhatToGrow);
                msgWhatToGrow.setPositiveButton(R.string.btnThanks, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

                msgWhatToGrow.show();
            }
        });

        btnPottingPlants.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder msgPottingPlants = new AlertDialog.Builder(TipsScreen.this);
                msgPottingPlants.setTitle(R.string.btnPottingPlants);
                msgPottingPlants.setMessage(R.string.txtPottingPlants);
                msgPottingPlants.setPositiveButton(R.string.btnThanks, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

                msgPottingPlants.show();
            }
        });

        btnGardenCare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder msgGardenCare = new AlertDialog.Builder(TipsScreen.this);
                msgGardenCare.setTitle(R.string.btnGardenCare);
                msgGardenCare.setMessage(R.string.txtGardenCare);
                msgGardenCare.setPositiveButton(R.string.btnThanks, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

                msgGardenCare.show();
            }
        });
    }

    /**
     * On Pause the screen will be finished and removed from the stack
     */
    @Override
    protected void onPause()
    {
        super.onPause();
        finish();
    }
}
