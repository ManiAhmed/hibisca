package com.hibisca.transformers.hibisca.screens;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.hibisca.transformers.hibisca.MenuActivity;
import com.hibisca.transformers.hibisca.R;
import com.hibisca.transformers.hibisca.fragments.AddPlantFragment;
import com.hibisca.transformers.hibisca.pojo.Plant;
import com.hibisca.transformers.hibisca.pojo.UserSession;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class PlantLocationsScreen extends MenuActivity implements OnMapReadyCallback {
    private GoogleMap mMap;
    private List<Marker> markerList;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plant_locations_screen);
        if (markerList == null) {
            markerList = new ArrayList<Marker>();
        }
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        sharedPreferences = getPreferences(MODE_PRIVATE);
        editor = sharedPreferences.edit();
        mMap = googleMap;
        LoadPreferencesMarkers();
        zoomOnUserLocation();



        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
          @Override
          public boolean onMarkerClick(Marker marker) {
              marker.showInfoWindow();
              LatLng position = marker.getPosition();
              getWeatherData(PlantLocationsScreen.this, Double.toString(position.latitude), Double.toString(position.longitude));
              return true;
          }
        });

        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {

            @Override
            public void onMarkerDragStart(Marker marker) {
                markerList.remove(markerList.indexOf(marker));
                editor.remove(marker.getTitle());
                editor.remove(Double.toString(marker.getPosition().latitude));
                editor.remove(Double.toString(marker.getPosition().longitude));
                editor.commit();
                marker.remove();

            }

            @Override
            public void onMarkerDrag(Marker marker) {
            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
            }
        });

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener()
        {
            @Override
            public void onMapClick(final LatLng arg0)
            {
                AlertDialog alertDialog = new AlertDialog.Builder(PlantLocationsScreen.this).create();
                alertDialog.setTitle(R.string.titleMap);
                alertDialog.setMessage(getString(R.string.dialogMessageMap));

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

                alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.btnViewWeather),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                getWeatherData(PlantLocationsScreen.this, Double.toString(arg0.latitude), Double.toString(arg0.longitude));
                                dialog.dismiss();
                            }
                });

                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.btnSetMarker), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        setMarker(arg0);
                    }
                });
                
                alertDialog.show();
            }
        });
    }

    /**
     * Centers the map on the users location if the permission is allowed
     */
    private void zoomOnUserLocation() {
        LocationManager lm = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();

        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) ==
                PackageManager.PERMISSION_GRANTED &&
                ContextCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) ==
                        PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);

            Location location = lm.getLastKnownLocation(LocationManager.GPS_PROVIDER);
            if(location == null) {
                criteria.setAccuracy(Criteria.ACCURACY_COARSE);

                String provider = lm.getBestProvider(criteria, true);
                location = lm.getLastKnownLocation(provider);
            }

            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 13));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(location.getLatitude(), location.getLongitude()))
                    .zoom(17)
                    .bearing(90)
                    .tilt(40)
                    .build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }

        else {
            ActivityCompat.requestPermissions(this, new String[] {
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION },
                    8888);
        }
    }

    /**
     * Sets a marker on the map
     * @param arg0 the latlon for the marker
     */
    private void setMarker(final LatLng arg0) {
        final View view;
        AlertDialog alertDialogMarker = new AlertDialog.Builder(PlantLocationsScreen.this).create();
        LayoutInflater inflater = getLayoutInflater();
        view = inflater.inflate(R.layout.add_marker_dialog_box_layout, null);
        alertDialogMarker.setView(view);

        alertDialogMarker.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.add), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {

                EditText markerName = (EditText) view.findViewById(R.id.editTextMarkerName);
                String markerTitle = markerName.getText().toString();

                markerList.add(mMap.addMarker
                        (new MarkerOptions()
                        .position(new LatLng(arg0.latitude, arg0.longitude))
                        .title(markerTitle)
                        .draggable(true)));

            }
        });

        alertDialogMarker.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alertDialogMarker.show();


    }

    /**
     * Calls OWMAPI for the weather of the desired lat lon and displays it
     * @param context the context
     * @param lat the latitude
     * @param lon the longitude
     */
    @SuppressLint("StaticFieldLeak")
    public void getWeatherData(final Context context, final String lat, final String lon) {
        final JSONObject[] data = {null};

        new AsyncTask<Void, Void, JSONObject[]>() {


            @Override
            protected void onPreExecute() {
                super.onPreExecute();

            }

            @Override
            protected JSONObject[] doInBackground(Void... params) {
                try {
                    URL url = new URL("http://openweathermap.org/data/2.5/weather?lat="+lat+"&lon="+lon+"&appid=b6907d289e10d714a6e88b30761fae22");

                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                    BufferedReader reader =
                            new BufferedReader(new InputStreamReader(connection.getInputStream()));

                    StringBuffer json = new StringBuffer(1024);
                    String tmp = "";

                    while((tmp = reader.readLine()) != null)
                        json.append(tmp).append("\n");
                    reader.close();

                    data[0] = new JSONObject(json.toString());

                    if(data[0].getInt("cod") != 200) {
                        Log.d("Error", "Request Cancelled");
                    }


                } catch (Exception e) {

                    System.out.println("Exception "+ e.getMessage());
                    return null;
                }

                return data;
            }

            @Override
            protected void onPostExecute(JSONObject[] data) {
                if(data!=null) {
                    TextView weatherTxtView = (TextView) PlantLocationsScreen.this.findViewById(R.id.weatherDesc);
                    TextView locationNameTxtView = (TextView) PlantLocationsScreen.this.findViewById(R.id.locationName);
                    TextView tempTxtView = (TextView) PlantLocationsScreen.this.findViewById(R.id.tempTxt);
                    TextView pressureTxtView = (TextView) PlantLocationsScreen.this.findViewById(R.id.pressure);
                    TextView humidityTxtView = (TextView) PlantLocationsScreen.this.findViewById(R.id.humidity);


                    JSONArray weatherArray = data[0].optJSONArray("weather");
                    JSONObject weather = (JSONObject) weatherArray.opt(0);
                    JSONObject base = data[0].optJSONObject("base");
                    JSONObject mainWeather = data[0].optJSONObject("main");

                    Log.d("things", mainWeather.optInt("temp") + " " + mainWeather.optInt("pressure") + " " + mainWeather.optInt("humidity") + " " +
                            weather.optString("main"));

                    tempTxtView.setText(mainWeather.optInt("temp") + "");
                    pressureTxtView.setText(mainWeather.optInt("pressure") +"");
                    humidityTxtView.setText(mainWeather.optInt("humidity") +"");
                    weatherTxtView.setText(weather.optString("main"));

                    locationNameTxtView.setText(data[0].optString("name"));
                }
            }
        }.execute();
    }

    /**
     * Saves the user's markers using sharedPreferences
     */
    private void SavePreferencesMarkers() {
        sharedPreferences = getPreferences(MODE_PRIVATE);
        editor = sharedPreferences.edit();

        editor.putInt("listSize", markerList.size());

        for(int i = 0; i <markerList.size(); i++){
            editor.putFloat("lat"+i, (float) markerList.get(i).getPosition().latitude);
            editor.putFloat("long"+i, (float) markerList.get(i).getPosition().longitude);
            editor.putString("title"+i, markerList.get(i).getTitle());
        }

        editor.commit();
    }

    /**
     * Loads the user's markers saved in sharedPreferences
     */
    private void LoadPreferencesMarkers() {
        sharedPreferences = getPreferences(MODE_PRIVATE);

        int size = sharedPreferences.getInt("listSize", 0);
        for(int i = 0; i < size; i++){
            double lat = (double) sharedPreferences.getFloat("lat"+i,0);
            double longit = (double) sharedPreferences.getFloat("long"+i,0);
            String title = sharedPreferences.getString("title"+i,"NULL");

            markerList.add(mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(lat, longit))
                    .title(title)
                    .draggable(true)));
        }
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        SavePreferencesMarkers();
    }

    @Override
    public void onPause() {
        super.onPause();
        SavePreferencesMarkers();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        SavePreferencesMarkers();
    }

    @Override
    public void onStop() {
        super.onStop();
        SavePreferencesMarkers();
    }
}
