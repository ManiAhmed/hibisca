package com.hibisca.transformers.hibisca.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hibisca.transformers.hibisca.pojo.Plant;
import com.hibisca.transformers.hibisca.R;
import com.hibisca.transformers.hibisca.screens.PlantInfoScreen;

import java.util.List;

/**
 * Custom adapter used to get data on plants associated to a user and display them using a GridView
 */

public class MyPlantsGridAdapter extends ArrayAdapter {

    private Context context;
    private List<Plant> plants;

    /**
     * Constructor for Plants Grid adapter
     * @param context context of the calling activity
     * @param plants list of user's plants
     */
    public MyPlantsGridAdapter(Context context, List<Plant> plants) {
        super(context, 0, plants);
        this.context = context;
        this.plants = plants;
    }

    /**
     * Returns a grid view containing a list of the users plants
     * @param position position the specified index
     * @param convertView convertView the view to be changed
     * @param parent parent the parent view
     * @return the grid view containing user's plants
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.plants_grid_view, parent, false);
        }

        TextView plantName = (TextView) convertView.findViewById(R.id.grid_text);
        ImageView plantImage = (ImageView) convertView.findViewById(R.id.grid_image);
        final Plant plant = this.plants.get(position);
        plantName.setText(plant.getPlantName());
        plantImage.setImageResource(R.drawable.ic_plants);
        plantImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent plantInfoIntent = new Intent(context, PlantInfoScreen.class);
                plantInfoIntent.putExtra("Spcies", plant.getSpecies());
                plantInfoIntent.putExtra("ID", plant.getPlantID());
                plantInfoIntent.putExtra("PlantName", plant.getPlantName());
                context.startActivity(plantInfoIntent);
            }
        });

        return convertView;
    }
}
