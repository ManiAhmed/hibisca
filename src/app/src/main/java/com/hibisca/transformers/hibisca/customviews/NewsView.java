package com.hibisca.transformers.hibisca.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.text.Layout;
import android.text.StaticLayout;
import android.view.View;
import android.util.AttributeSet;

import com.hibisca.transformers.hibisca.R;

/**
 * Created by cstuser on 5/2/2018.
 */

public class NewsView extends View {
    private Bitmap newsImage;

    private String title;
    private String source;
    private String contentDesc;

    private int imageHolderColor;
    private int titleColor;
    private int sourceColor;
    private int contentColor;

    private Paint paint;
    private android.text.TextPaint graphicsPaint;
    private Bitmap newsBitmap;
    private Bitmap bitmap;

    /**
     * Creates a new NewsView view
     * @param context context of the calling activity
     * @param attributeSet the set of attributes defined in the XML
     */
    public NewsView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        paint = new Paint();
        TypedArray typedArray = context.getTheme().obtainStyledAttributes(attributeSet, R.styleable.NewsView, 0, 0);
        try {
            newsBitmap = Bitmap.createScaledBitmap(BitmapFactory.decodeResource(getResources(), R.drawable.hibisca_logo), 220, 200, false);

            title = typedArray.getString(R.styleable.NewsView_title);
            source = typedArray.getString(R.styleable.NewsView_source);
            contentDesc = typedArray.getString(R.styleable.NewsView_contentDesc);

            imageHolderColor = typedArray.getInteger(R.styleable.NewsView_imageHolderColor, Color.GRAY);
            titleColor = typedArray.getInteger(R.styleable.NewsView_titleColor, Color.BLACK);
            sourceColor = typedArray.getInteger(R.styleable.NewsView_sourceColor, Color.BLACK);
            contentColor = typedArray.getInteger(R.styleable.NewsView_contentColor, Color.BLACK);
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            typedArray.recycle();
        }
    }

    /**
     * Draws the view on the canvas
     * @param canvas the canvas containing the view
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        try {
            int width = this.getMeasuredWidth()/5;
            int height = this.getMeasuredHeight()/2;
            paint.setAntiAlias(true);

            //Image holder

        /*int rad = (width > height)? height-40:width-40;
        paint.setColor(imageHolderColor);
        canvas.drawCircle(135, height, rad, paint);*/

            //Content
            paint.setFakeBoldText(false);
            paint.setColor(contentColor);
            paint.setTextSize(40);
            canvas.drawText(contentDesc, width+110, height-20, paint);

            //Source
            paint.setFakeBoldText(false);
            paint.setColor(sourceColor);
            paint.setTextSize(40);
            canvas.drawText(source, width+110, (height*2)-20, paint);


            //Title
            paint.setFakeBoldText(true);
            paint.setColor(titleColor);
            paint.setTextSize(40);
            canvas.drawText(title, width+110, height/2, paint);

            //Image
            paint.setFakeBoldText(false);
            if(getNewsImage() != null)
                bitmap = Bitmap.createScaledBitmap(getNewsImage(), 220, 200, false);
            else bitmap = newsBitmap;
            canvas.drawBitmap(bitmap, 35, height-100, paint);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Retrieves the image corresponding to the news view
     * @return
     */
    public Bitmap getNewsImage() {
        try {
            return newsImage;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Sets the image corresponding to the news view
     * @param newsImage image corresponding to the news view
     */
    public void setNewsImage(Bitmap newsImage) {
        try {
            this.newsImage = newsImage;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the title corresponding to the news view
     * @return title corresponding to the news view
     */
    public String getTitle() {
        try {
            return title;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Sets the title corresponding to the news view
     * @param title title corresponding to the news view
     */
    public void setTitle(String title) {
        try {
            this.title = title;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the source text of the corresponding view
     * @return source text of the corresponding view
     */
    public String getSource() {
        try {
            return source;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Sets the source text of the corresponding view
     * @param source source text of the corresponding view
     */
    public void setSource(String source) {
        try {
            this.source = source;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Gets the content description text of the corresponding to the news view
     * @return contentDesc content description text
     */
    public String getContentDesc() {
        try {
            return contentDesc;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Sets the content description text of the corresponding to the news view
     * @param contentDesc content description text
     */
    public void setContentDesc(String contentDesc) {
        try {
            this.contentDesc = contentDesc;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Retrieves the color of the image holder
     * @return color of the image holder
     */
    public int getImageHolderColor() {
        try {
            return imageHolderColor;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * Sets the color of the image holder
     * @param imageHolderColor color of the image holder
     */
    public void setImageHolderColor(int imageHolderColor) {
        try {
            this.imageHolderColor = imageHolderColor;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Retrieves the color of the title text
     * @return color of the title text
     */
    public int getTitleColor() {
        try {
            return titleColor;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * Sets the color of the title text
     * @param titleColor color of the title text
     */
    public void setTitleColor(int titleColor) {
        try {
            this.titleColor = titleColor;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Retrieves the color of the source text
     * @return color of the source text
     */
    public int getSourceColor() {
        try {
            return sourceColor;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * Sets the color of the news source text
     * @param sourceColor the color to set the source text to
     */
    public void setSourceColor(int sourceColor) {
        try {
            this.sourceColor = sourceColor;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Retrieves the color of the news content text
     * @return the color the content text is set to
     */
    public int getContentColor() {
        try {
            return contentColor;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * Set the color of the news content text
     * @param contentColor color to set the content text to
     */
    public void setContentColor(int contentColor) {
        try {
            this.contentColor = contentColor;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
