package com.hibisca.transformers.hibisca.screens;


import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.graphics.Color;

import android.media.MediaPlayer;
import android.support.constraint.ConstraintLayout;
import android.os.Bundle;

import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;


import com.hibisca.transformers.hibisca.MenuActivity;
import com.hibisca.transformers.hibisca.R;

/**
 * Welcomes the user to the application and instructs
 * then on how to proceed using/starting the application.
 * For a enhanced user experience it also includes some
 * animations for fading the images in and out while the
 * background changes colors and a sound plays upon reaching this
 * page.
 */
public class WelcomeScreen extends MenuActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_screen);
        setTitle("");
        final MediaPlayer wlcm = MediaPlayer.create(this, R.raw.welcome);
        wlcm.start();


        ImageView imgV = (ImageView)findViewById(R.id.hibiscaFlower);
        TextView  welcm = (TextView)findViewById(R.id.welcome);
        TextView  instrctn = (TextView)findViewById(R.id.clickMenu);
        ImageView strtA = (ImageView)findViewById(R.id.startArrow);

        ConstraintLayout ws = (ConstraintLayout)findViewById(R.id.welcomeLayout);

        ObjectAnimator colorFade = ObjectAnimator.ofObject(ws, "backgroundColor", new ArgbEvaluator(), Color.argb(255,209,243,252), 0xffffffff);
        colorFade.setDuration(5500);
        colorFade.setRepeatCount(1);
        colorFade.setRepeatMode(ObjectAnimator.REVERSE);
        colorFade.start();

        Animation fadeIn = new AlphaAnimation(0, 1);
        fadeIn.setInterpolator(new DecelerateInterpolator());
        fadeIn.setDuration(6000);

        Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new AccelerateInterpolator());
        fadeOut.setStartOffset(5000);
        fadeOut.setDuration(4000);
        fadeOut.setFillAfter(true);

        AnimationSet animation = new AnimationSet(false);
        animation.addAnimation(fadeIn);
        animation.addAnimation(fadeOut);

        Animation fadeIn2 = new AlphaAnimation(0, 1);
        fadeIn2.setInterpolator(new DecelerateInterpolator());
        fadeIn2.setStartOffset(3000);
        fadeIn2.setDuration(6000);

        Animation fadeIn3 = new AlphaAnimation(0, 0);
        fadeIn3.setInterpolator(new DecelerateInterpolator());
        fadeIn3.setDuration(3000);

        AnimationSet animation2 = new AnimationSet(false);
        animation2.addAnimation(fadeIn3);
        animation2.addAnimation(fadeIn2);

        imgV.setAnimation(animation);
        animation.setFillAfter(true);
        welcm.setAnimation(animation);
        instrctn.setAnimation(animation);

        strtA.setAnimation(animation2);
    }

    /**
     * This involves a "finish" command that
     * willl be needed when the user intends to
     * "log out" of the appication from their profile
     * screen.
     */
    @Override
    protected void onPause()
    {
        super.onPause();
    }
}

